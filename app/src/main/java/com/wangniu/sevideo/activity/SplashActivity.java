package com.wangniu.sevideo.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.wangniu.sevideo.R;


/**
 * Created by Administrator on 2016/12/16 0016.
 */
public class SplashActivity extends Activity {

    Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            jumptoMain();
            finish();
        }
    };

    private void jumptoMain(){
        Intent intent = new Intent (this,MainActivity.class);
        startActivity(intent);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actvity_splash);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mHandler.sendEmptyMessageDelayed(11,1000);
    }
}
