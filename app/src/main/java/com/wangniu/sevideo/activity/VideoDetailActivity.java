package com.wangniu.sevideo.activity;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.umeng.analytics.MobclickAgent;
import com.wangniu.sevideo.MyApplication;
import com.wangniu.sevideo.R;
import com.wangniu.sevideo.adapter.SpacesItemDecoration;
import com.wangniu.sevideo.adapter.VideoDetailAdapter;
import com.wangniu.sevideo.bean.DownloadInfo;
import com.wangniu.sevideo.db.BDDatabaseHelper;
import com.wangniu.sevideo.db.FileManager;
import com.wangniu.sevideo.request.CallJSONObjectRequest;
import com.wangniu.sevideo.request.CallRequestUtils;
import com.wangniu.sevideo.util.JSONUtil;
import com.wangniu.sevideo.util.TheConstants;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2016/12/6 0006.
 */
public class VideoDetailActivity extends BaseActivity implements View.OnClickListener {

    private ImageButton btnBack;
    private ImageButton btnSetting;
    private RecyclerView rvContent;
    private VideoDetailAdapter mAdapter;
    private BDDatabaseHelper mHelper;
    private List<DownloadInfo> downloadInfos = new ArrayList<>();
    private final int GETDOWNLOADINFOS = 12458;
    private int defaultId = 0;

    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == GETDOWNLOADINFOS){
                mAdapter.setList(downloadInfos);
                mAdapter.notifyDataSetChanged();
            }
        }
    };
    @Override
    protected void initVariables() {
        mAdapter = new VideoDetailAdapter(this);
        mHelper = new BDDatabaseHelper(this);
        DownloadInfo info = FileManager.queryById(MyApplication.getSharedPreferences().getInt(TheConstants.SELECTID, 0));
        if (info!=null){
            defaultId = info.getId();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DownloadInfo info = FileManager.queryById(MyApplication.getSharedPreferences().getInt(TheConstants.SELECTID, 0));
        if (info!=null && info.getId()!=defaultId){
//            MainActivity.getInstance().update();
        }
    }

    @Override
    protected void initViews() {
        setContentView(R.layout.activity_video_detail);

        btnBack = (ImageButton) findViewById(R.id.btn_back);
        btnBack.setOnClickListener(this);
        btnSetting = (ImageButton) findViewById(R.id.btn_setting);
        btnSetting.setOnClickListener(this);

        rvContent = (RecyclerView) findViewById(R.id.rv_content);
        final GridLayoutManager manager = new GridLayoutManager(this,2);
        rvContent.setLayoutManager(manager);
        rvContent.setAdapter(mAdapter);
        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return mAdapter.isHeader(position) ? manager.getSpanCount() : 1;
            }
        });
        SpacesItemDecoration itemDecoration = new SpacesItemDecoration(14);
        rvContent.addItemDecoration(itemDecoration);
    }

    @Override
    protected void loadData() {
        getDownloadInfo();
    }

    public void getDownloadInfo() {
        Map<String, String> reqParams = CallRequestUtils.getDownloadInfoParams();
        CallJSONObjectRequest objRequest = new CallJSONObjectRequest(Request.Method.POST,
                TheConstants.URL_GET_VIDEO_INFOS, reqParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        JSONObject[] dataArray = JSONUtil.getJSONArray2(jsonObject, "data");
                        if (dataArray != null) {
                            downloadInfos.clear();
                            for (JSONObject data : dataArray) {
                                int id = JSONUtil.getInt(data, "id");
                                String url = JSONUtil.getString(data, "url");
                                String name = JSONUtil.getString(data, "name");
                                String img = JSONUtil.getString(data, "img");
                                DownloadInfo downloadInfo = new DownloadInfo();
                                downloadInfo.setName(name);
                                downloadInfo.setUrl(url);
                                downloadInfo.setId(id);
                                downloadInfo.setImg(img);
                                downloadInfos.add(downloadInfo);
                            }
                            handler.sendEmptyMessage(GETDOWNLOADINFOS);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.e("==Error==", volleyError.toString());
                        if (volleyError instanceof TimeoutError) {
                            Toast.makeText(MyApplication.getInstance(),
                                    MyApplication.getInstance().getString(R.string.error_network_timeout), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MyApplication.getInstance(),
                                    MyApplication.getInstance().getString(R.string.error_network_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                });
        MyApplication.getInstance().addToRequestQueue(objRequest, "");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_back:
                finish();
                break;
            case R.id.btn_setting:
                MobclickAgent.onEvent(VideoDetailActivity.this, "Ring_03");
                Intent intent = new Intent(this,SettingActivity.class);
                startActivity(intent);
                break;
        }
    }
}
