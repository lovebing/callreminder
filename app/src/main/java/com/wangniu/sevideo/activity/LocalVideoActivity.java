package com.wangniu.sevideo.activity;

import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wangniu.sevideo.R;
import com.wangniu.sevideo.adapter.SpacesItemDownloadDecoration;
import com.wangniu.sevideo.adapter.VideoLocalAdapter;
import com.wangniu.sevideo.bean.VideoInfo;
import com.wangniu.sevideo.reciver.AbstructProvider;
import com.wangniu.sevideo.reciver.VideoProvider;

import java.util.List;

/**
 * Created by Administrator on 2017/1/3 0003.
 */
public class LocalVideoActivity extends BaseActivity implements View.OnClickListener {
    private ImageButton btnBack;
    private TextView tvTitle;
    private RecyclerView rvContent;
    private VideoLocalAdapter mAdapter;
    private List<VideoInfo> infos;
    private RelativeLayout rlProgress;

    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            rlProgress.setVisibility(View.GONE);
            mAdapter.setList(infos);
            mAdapter.notifyDataSetChanged();
        }
    };

    @Override
    protected void initVariables() {
        mAdapter = new VideoLocalAdapter(this);
    }

    @Override
    protected void initViews() {
        setContentView(R.layout.activity_download);
        btnBack = (ImageButton) findViewById(R.id.btn_back);
        btnBack.setOnClickListener(this);
        tvTitle = (TextView) findViewById(R.id.tv_title);
        tvTitle.setText("本地视频");
        rvContent = (RecyclerView) findViewById(R.id.rv_content);
        final GridLayoutManager manager = new GridLayoutManager(this,2);
        rvContent.setLayoutManager(manager);
        rvContent.setAdapter(mAdapter);
        SpacesItemDownloadDecoration itemDecoration = new SpacesItemDownloadDecoration(16);
        rvContent.addItemDecoration(itemDecoration);
        rlProgress = (RelativeLayout) findViewById(R.id.rl_wait_progress);
        rlProgress.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadData() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                AbstructProvider provider = new VideoProvider(LocalVideoActivity.this);
                List<VideoInfo>listVideos = provider.getList();
                Message msg = Message.obtain();
                msg.what = 11111;
                infos = listVideos;
                handler.sendMessage(msg);
            }
        }).start();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_back:
                finish();
                break;
        }
    }
}
