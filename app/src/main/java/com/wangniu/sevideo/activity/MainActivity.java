package com.wangniu.sevideo.activity;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.dinuscxj.progressbar.CircleProgressBar;
import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListener;
import com.thin.downloadmanager.ThinDownloadManager;
import com.umeng.analytics.MobclickAgent;
import com.wangniu.sevideo.MyApplication;
import com.wangniu.sevideo.R;
import com.wangniu.sevideo.bean.DownloadInfo;
import com.wangniu.sevideo.bean.LocalContact;
import com.wangniu.sevideo.bean.OrderPaymentStatusBean;
import com.wangniu.sevideo.db.FileManager;
import com.wangniu.sevideo.dialog.GoSettingDialog;
import com.wangniu.sevideo.dialog.PayDialog;
import com.wangniu.sevideo.dialog.PraiseDialog;
import com.wangniu.sevideo.request.CallJSONObjectRequest;
import com.wangniu.sevideo.request.CallRequestUtils;
import com.wangniu.sevideo.service.KeepLiveService;
import com.wangniu.sevideo.util.AndroidUtil;
import com.wangniu.sevideo.util.CallUtils;
import com.wangniu.sevideo.util.GoToSettingUtil;
import com.wangniu.sevideo.util.JSONUtil;
import com.wangniu.sevideo.util.MeasureUtil;
import com.wangniu.sevideo.util.PermissionUtil;
import com.wangniu.sevideo.util.PermissionsCheckerUtil;
import com.wangniu.sevideo.util.StringUtil;
import com.wangniu.sevideo.util.TheConstants;
import com.wangniu.sevideo.util.WechatPayUtils;

import org.json.JSONObject;
import org.lovebing.android.oem.Vendor;
import org.lovebing.android.oem.VendorFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class MainActivity extends Activity implements View.OnClickListener ,GestureDetector.OnGestureListener{

    private SurfaceView mSurfaceView;
    private MediaPlayer mMedia;
    private SurfaceHolder holder;
    private Button btnNext;
    private ImageButton btnSettings;
    private ImageButton btnSelect;
    private Button btnSet;
    private List<DownloadInfo> downloadInfos = new ArrayList<>();
    private final int GETDOWNLOADINFOS = 13232;
    private final int UPDATANAME = 19800;
    private int selectedNum = -1;
    private int mainBgNum = 0;
    private SharedPreferences mShare;
    private String savePath;
    private CircleProgressBar circleView;
    private RelativeLayout waitProgress;
    private LinearLayout llMain;
    private TextView tvName;
    private RelativeLayout rlShow;
    private boolean canNext = true;
    private PraiseDialog mDialog;
    private GoSettingDialog toSettingDialog;
    private PayDialog payDialog;
    private OrderPaymentStatusBean paymentBean;
    private Button btnToNext;
    private Button btnToPrevious;
    private int defaultId;

    private String strImei;
    private String strImsi;
    private String strMacAddress;
    private String strUserId;
    private GestureDetector detector;
    private Random mRandom;
    private final int UPDATENAME = 17898895;

//    private BDDatabaseHelper mHelper;



    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == GETDOWNLOADINFOS) {
                waitProgress.setVisibility(View.GONE);
                int type = (int) msg.obj;
                if (type==TONEXT){
                    selectedNum =0;
                }else if (type==TOPREVIOUS){
                    selectedNum = downloadInfos.size()-1;
                }
                Log.e("==data==", "coming");
//                mHelper.add(downloadInfos.get(0));
//                mHelper.add(downloadInfos.get(1));
                videoNext(downloadInfos.get(selectedNum).getUrl());
            }else if(msg.what == UPDATANAME){
                if (selectedNum == -1){
                    tvName.setText("");
                }else{
                    tvName.setText(downloadInfos.get(selectedNum).getName());
                }
            }else if(msg.what == GoSettingDialog.TOSETTINGHOME){
                GoToSettingUtil.goToSetting(MainActivity.this);
            }else if (msg.what == TheConstants.MSG_PAYMENT_WXPAY_REQUIRED){
                WechatPayUtils.pay(paymentBean);
            }else if (msg.what == PayDialog.TOPAY){
                MobclickAgent.onEvent(MainActivity.this,"cr_pay");
                placeVIPOrder();
            }else if (msg.what ==  GoSettingDialog.TOSETTINGWIDOWN){
                Intent intent1 = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent1, REQUEST_CODE);
            }else if(msg.what ==  UPDATENAME){
               String name = (String) msg.obj;
                Log.e("coming",name);
               tvName.setText(name);
            }
        }
    };

    private final int REQUEST_CODE = 0;
    private final String[] PERMISSIONS = new String[]{
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.PROCESS_OUTGOING_CALLS,
            Manifest.permission.CALL_PHONE,
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mShare = MyApplication.getSharedPreferences();
        savePath = getExternalCacheDir().toString().toString();
        detector = new GestureDetector(this,this);
//        mHelper  =  new BDDatabaseHelper(this);
        DownloadInfo info = FileManager.queryById(MyApplication.getSharedPreferences().getInt(TheConstants.SELECTID, 0));
        if (info!=null){
            defaultId = info.getId();
        }
        initView();
        initDate();
//        initWatchDog();
        //注册广播
        registerReceiver(mHomeKeyEventReceiver, new IntentFilter(
                Intent.ACTION_CLOSE_SYSTEM_DIALOGS));
        Intent intent = new Intent(this,KeepLiveService.class);
        startService(intent);

        // PermissionUtil.checkPermission(this);
    }

//    private void initWatchDog() {
//        Subprocess.create(this, WatchDog.class);
//    }

//    }

    private void initDate() {
        if ("".equals(MyApplication.getSharedPreferences().getString("UserId", ""))){
        StringBuilder sb = new StringBuilder();
        strImei = AndroidUtil.getImei(this);
        strImsi = AndroidUtil.getImsi(this);
        strMacAddress = "";
        WifiManager wifiMgr = (WifiManager)getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = (null == wifiMgr ? null : wifiMgr.getConnectionInfo());
        if (null != info) {
            strMacAddress = info.getMacAddress();
        }
        String strModel = Build.MODEL;
        String strUserId = StringUtil.getMd5Value(sb.append(strImei).append("#")
                .append(strMacAddress).append("#").append(strModel).append(AndroidUtil.getPackageName(this)).toString());
        MyApplication.getSharedPreferences().edit().putString("UserId",strUserId).commit();
        }

        mHandler.sendEmptyMessage(189);

        new Thread(new Runnable() {
            @Override
            public void run() {
                FileManager.deleteAllContact();
                List<LocalContact> localContacts = CallUtils.getContats(MainActivity.this);
                FileManager.addContants(localContacts);
            }
        }).start();
        localInfos = FileManager.getAllInfo();

    }

    private void initView() {
        mSurfaceView = (SurfaceView) findViewById(R.id.sv_show);
        mMedia = new MediaPlayer();
        mMedia.setOnInfoListener(new MediaPlayer.OnInfoListener() {
            @Override
            public boolean onInfo(MediaPlayer mp, int what, int extra) {
                switch (what) {
                    case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                        waitProgress.setVisibility(View.VISIBLE);
                        circleView.setVisibility(View.GONE);
                        break;
                    case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                        waitProgress.setVisibility(View.GONE);
                        break;
                }
                return false;
            }
        });
        mMedia.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                waitProgress.setVisibility(View.GONE);
            }
        });

        mMedia.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
            @Override
            public void onBufferingUpdate(MediaPlayer mp, int percent) {
                Log.e("BufferingUpdate",percent+"");
            }
        });
        holder = mSurfaceView.getHolder();
        myCallBack = new MyCallBack("default.mp4");
        holder.addCallback(myCallBack);

        btnNext = (Button) findViewById(R.id.btn_next);
        btnNext.setOnClickListener(this);
        btnSettings = (ImageButton) findViewById(R.id.btn_settings);
        btnSettings.setOnClickListener(this);
        btnSet = (Button) findViewById(R.id.btn_set_video);
        btnSet.setOnClickListener(this);

        circleView = (CircleProgressBar) findViewById(R.id.line_progress);
        circleView.setMax(100);

        waitProgress = (RelativeLayout) findViewById(R.id.rl_wait_progress);

        llMain = (LinearLayout) findViewById(R.id.ll_main);

        tvName = (TextView) findViewById(R.id.tv_name);

        rlShow = (RelativeLayout) findViewById(R.id.rl_video_show);
        List<Integer> widownSize = MeasureUtil.getWidownSize(this);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) rlShow.getLayoutParams();
        params.width = widownSize.get(0)*3/4;
        params.height = widownSize.get(1)*3/4;
        rlShow.setLayoutParams(params);

        btnSelect = (ImageButton) findViewById(R.id.btn_selected);
        btnSelect.setOnClickListener(this);

        btnNext = (Button) findViewById(R.id.btn_to_next);
        btnToPrevious = (Button) findViewById(R.id.btn_to_previous);
        btnNext.setOnClickListener(this);
        btnToPrevious.setOnClickListener(this);


    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_to_next:
                MobclickAgent.onEvent(MainActivity.this, "Ring_07");
                replaceVideo(TONEXT);
                break;
            case R.id.btn_to_previous:
                MobclickAgent.onEvent(MainActivity.this, "Ring_07");
                replaceVideo(TOPREVIOUS);
                break;
            case R.id.btn_next:
                MobclickAgent.onEvent(MainActivity.this, "Ring_09");
                Intent toDetail = new Intent(this,VideoDetailActivity.class);
                startActivity(toDetail);
                break;
            case R.id.btn_settings:
                MobclickAgent.onEvent(MainActivity.this, "Ring_03");
                Intent intent = new Intent(this, SettingActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_set_video:
                Vendor vendor = VendorFactory.getVendor(this);
                MobclickAgent.onEvent(MainActivity.this, "Ring_08");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (PermissionsCheckerUtil.lacksPermissions(PERMISSIONS,this)){
                        if (toSettingDialog==null){
                            toSettingDialog = new GoSettingDialog(MainActivity.this,mHandler, 0);
                        }
                        toSettingDialog.show();
                    }else if (!Settings.canDrawOverlays(MainActivity.this)){
                        toSettingDialog = new GoSettingDialog(MainActivity.this,mHandler,1);
                        toSettingDialog.show();
                    } else if(!vendor.notificationListenersEnabled() || !vendor.getName().equals(Vendor.Name.GENERAL)) {
                        toSettingDialog = new GoSettingDialog(MainActivity.this,mHandler,2);
                        toSettingDialog.show();
                    }
                }
//                else{
//                    toSettingDialog = new GoSettingDialog(MainActivity.this,mHandler,1);
//                    toSettingDialog.show();
//                }
                if (selectedNum==-1){
                    DownloadInfo info = FileManager.queryById(MyApplication.getSharedPreferences().getInt(TheConstants.SELECTID, 0));
                    if (info!=null){
                        mShare.edit().putInt(TheConstants.SELECTID, info.getId()).commit();
                        mShare.edit().putBoolean(TheConstants.SWITCHONOFF, true).commit();
                    }else{
                        mShare.edit().putString(TheConstants.RINGSAVEPATH, "default").commit();
                        mShare.edit().putBoolean(TheConstants.SWITCHONOFF, true).commit();
                    }
                    Toast.makeText(MainActivity.this, "铃声设置成功", Toast.LENGTH_SHORT).show();
                }else{
                    DownloadInfo info = FileManager.queryById(downloadInfos.get(selectedNum).getId());
                    if (info!=null){
                        mShare.edit().putInt(TheConstants.SELECTID, info.getId()).commit();
                        mShare.edit().putBoolean(TheConstants.SWITCHONOFF, true).commit();
                        Toast.makeText(MainActivity.this, "铃声设置成功", Toast.LENGTH_SHORT).show();
                    }else{
                        dowloadVideo(downloadInfos.get(selectedNum).getUrl(), savePath
                                + downloadInfos.get(selectedNum).getId() + ".mp4");
                    }

                }
                break;

            case R.id.btn_selected:
                MobclickAgent.onEvent(MainActivity.this,"Ring_01");
                Intent toLocal = new Intent(this,LocalVideoActivity.class);
                startActivity(toLocal);
//                if (mDialog == null){
//                    mDialog = new PraiseDialog(this,0);
//                }
//                mDialog.show();
//                Intent localVideo = new Intent();
//                localVideo.setType("video/*");
//                localVideo.setAction(Intent.ACTION_GET_CONTENT);
//                startActivityForResult(localVideo, 1);
//                AbstructProvider provider = new VideoProvider(this);
//                List<VideoInfo>listVideos = provider.getList();
//                for (VideoInfo videoInfo : listVideos){
//                    Log.e("video==please",videoInfo.toString());
//                    Log.e("video==please",(getVideoThumbnail1(videoInfo.getPath())==null)+"");
//                }
                break;
        }
    }



    private static final int TONEXT = 183;
    private static final int TOPREVIOUS = 123;
    private void replaceVideo(int type){
        if (isFirst){
            waitProgress.setVisibility(View.VISIBLE);
            circleView.setVisibility(View.GONE);
            isFirst =false;
            if (mMedia.isPlaying()){
                mMedia.stop();
            }
            getDownloadInfo(type);
        }else{
            if (canNext) {
                waitProgress.setVisibility(View.VISIBLE);
                circleView.setVisibility(View.GONE);
                if (type == TONEXT){
                    selectedNum = selectedNum + 1;
                    if (selectedNum >= downloadInfos.size()) {
                        selectedNum = -1;
                    }
                }else if (type == TOPREVIOUS){
                    selectedNum = selectedNum - 1;
                    if (selectedNum <-1) {
                        selectedNum = downloadInfos.size()-1;
                    }
                }

                mainBgNum = mainBgNum + 1;
                if (mainBgNum % 3 == 0) {
                    llMain.setBackgroundDrawable(getResources().getDrawable(R.mipmap.img_bg_main));
                } else if (mainBgNum % 3 == 1) {
                    llMain.setBackgroundDrawable(getResources().getDrawable(R.mipmap.img_bg_main1));
                } else if (mainBgNum % 3 == 2) {
                    llMain.setBackgroundDrawable(getResources().getDrawable(R.mipmap.img_bg_main2));
                }
                canNext = false;
                if (selectedNum == -1){
                    videoNext("default.mp4");
                }else{
                    videoNext(downloadInfos.get(selectedNum).getUrl());
                }
            }
        }
    }



    private static final int REQUEST_APP_SETTINGS = 168;
    private MyCallBack myCallBack;

    private synchronized void videoNext(final String url) {
        Log.e("videoNext","comging");
        new Thread(new Runnable() {
            @Override
            public void run() {
        mHandler.sendEmptyMessage(UPDATANAME);
        mMedia.reset();
        if (holder == null) {
            holder = mSurfaceView.getHolder();
        }
        if (myCallBack == null) {
            myCallBack = new MyCallBack(url);
            holder.addCallback(myCallBack);
        } else {
            Log.e("coming","1");
            myCallBack.changeDate(url);
        }
        myCallBack.surfaceCreated(holder);
        canNext = true;
            }
        }).start();
    }

    private boolean isFirst = true;

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }



    private class MyCallBack implements SurfaceHolder.Callback {
        String url;

        public MyCallBack(String url) {
            this.url = url;
        }

        public void changeDate(String url) {
            this.url = url;
        }

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
//            player = new MediaPlayer();
            mMedia.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mMedia.setDisplay(holder);
            // 设置显示视频显示在SurfaceView上
            try {
                if (selectedNum==-1){
                        DownloadInfo info = FileManager.queryById(MyApplication.getSharedPreferences().getInt(TheConstants.SELECTID, 0));
                        Log.e("coming","2");
                        if (info!=null){
                            Log.e("coming", "3");
//                            tvName.setText(info.getName());
//                            defaultId = info.getId();
                            Message msg = Message.obtain();
//                            Log.e("coming",info.getName());
                            msg.what = UPDATENAME;
                            msg.obj = info.getName();
                            mHandler.sendMessage(msg);
                            mMedia.setDataSource(info.getUrl()) ;
                        }else{
                            AssetFileDescriptor afd = getResources().openRawResourceFd(R.raw.dvideo);
                            mMedia.setDataSource(afd.getFileDescriptor(),afd.getStartOffset(), afd.getLength()) ;
                        }
                }else{
//                    String path = savePath
//                            + downloadInfos.get(selectedNum).getId() + ".mp4";
//                    File file = new File(path);
//                    if (file.exists()&&mShare.getBoolean(path,false)){
//                        mMedia.setDataSource(path);
//                    }else{
//                        mMedia.setDataSource(url);
//                    }
                    DownloadInfo info = FileManager.queryById(downloadInfos.get(selectedNum).getId());
                    if (info!=null){
                        mMedia.setDataSource(info.getUrl());
                    }else{
                        mMedia.setDataSource(url);
                    }
                }
                mMedia.setLooping(true);
                mMedia.prepare();
                mMedia.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {

        }
    }

    public void getDownloadInfo(final int type) {
        Map<String, String> reqParams = CallRequestUtils.getDownloadInfoParams();
        CallJSONObjectRequest objRequest = new CallJSONObjectRequest(Request.Method.POST,
                TheConstants.URL_GET_VIDEO_INFOS, reqParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        JSONObject[] dataArray = JSONUtil.getJSONArray2(jsonObject, "data");
                        if (dataArray != null) {
                            downloadInfos.clear();
                            for (JSONObject data : dataArray) {
                                int id = JSONUtil.getInt(data, "id");
                                String url = JSONUtil.getString(data, "url");
                                String name = JSONUtil.getString(data, "name");
                                String img = JSONUtil.getString(data, "img");
                                DownloadInfo downloadInfo = new DownloadInfo();
                                downloadInfo.setName(name);
                                downloadInfo.setUrl(url);
                                downloadInfo.setId(id);
                                downloadInfo.setImg(img);
                                downloadInfos.add(downloadInfo);
                            }
                            Message msg = Message.obtain();
                            msg.what = GETDOWNLOADINFOS;
                            msg.obj = type;
                            mHandler.sendMessage(msg);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.e("==Error==", volleyError.toString());
                        if (volleyError instanceof TimeoutError) {
                            Toast.makeText(MyApplication.getInstance(),
                                    MyApplication.getInstance().getString(R.string.error_network_timeout), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MyApplication.getInstance(),
                                    MyApplication.getInstance().getString(R.string.error_network_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                });
        MyApplication.getInstance().addToRequestQueue(objRequest, "");
    }

    private void dowloadVideo(String downloadstr, final String destinationstr) {
        File file = new File(destinationstr);
        if (file.exists()) {
            if (mShare.getBoolean(destinationstr,false)){
//                mShare.edit().putString(TheConstants.RINGSAVEPATH, destinationstr).commit();
//                mShare.edit().putString(TheConstants.RINGSAVENAME, downloadInfos.get(selectedNum).getName()).commit();
                mShare.edit().putString(TheConstants.RINGSAVEPATH, "").commit();
                mShare.edit().putInt(TheConstants.SELECTID, downloadInfos.get(selectedNum).getId()).commit();
                mShare.edit().putBoolean(TheConstants.SWITCHONOFF, true).commit();
                Toast.makeText(MainActivity.this, "铃声设置成功", Toast.LENGTH_SHORT).show();
            }else{
                circleView.setVisibility(View.VISIBLE);
            }
            return;
        }
        //付费逻辑
        if (mShare.getInt(TheConstants.SETTIME,-1)==1){
            if (payDialog == null){
                payDialog = new PayDialog(this,mHandler);
            }
            payDialog.show();
            return;
        }
        Uri downloadUri = Uri.parse(downloadstr);
        Uri destinationUri = Uri.parse(destinationstr);//this.getExternalCacheDir().toString()+"/test.mp4"
        circleView.setVisibility(View.VISIBLE);
        waitProgress.setVisibility(View.GONE);
        DownloadRequest downloadRequest = new DownloadRequest(downloadUri)
//                .addCustomHeader("Auth-Token", "YourTokenApiKey")
                .setRetryPolicy(new DefaultRetryPolicy())
                .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.HIGH)
                .setDownloadContext(this)//Optional
                .setDownloadListener(new DownloadStatusListener() {
                    @Override
                    public void onDownloadComplete(int id) {
                        Log.e("==downLoad==", "complete");
                        circleView.setVisibility(View.GONE);
                        DownloadInfo info_old = downloadInfos.get(selectedNum);
                        mShare.edit().putInt(TheConstants.SETTIME, mShare.getInt(TheConstants.SETTIME, -1)+1).commit();
                        //存储本地视频信息
                        DownloadInfo info_new = new DownloadInfo();
                        info_new.setId(info_old.getId());
                        info_new.setName(info_old.getName());
                        Log.e("downloadinfo",info_old.getImg());
                        info_new.setImg(info_old.getImg());
                        info_new.setUrl(destinationstr);
                        FileManager.addInfo(info_new);
//                        mShare.edit().putString(TheConstants.RINGSAVEPATH, destinationstr).commit();
//                        mShare.edit().putString(TheConstants.RINGSAVENAME, downloadInfos.get(selectedNum).getName()).commit();
                        mShare.edit().putString(TheConstants.RINGSAVEPATH, "").commit();
                        mShare.edit().putInt(TheConstants.SELECTID, downloadInfos.get(selectedNum).getId()).commit();
                        mShare.edit().putBoolean(TheConstants.SWITCHONOFF, true).commit();
                        mShare.edit().putInt(TheConstants.SETTIME, mShare.getInt(TheConstants.SETTIME, -1) + 1).commit();
                        mShare.edit().putBoolean(destinationstr, true).commit();
                        Toast.makeText(MainActivity.this, "铃声设置成功", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onDownloadFailed(int id, int errorCode, String errorMessage) {
                        Log.e("==downLoad==", "Failed" + errorMessage.toString());
                    }


                    @Override
                    public void onProgress(int id, long totalBytes, long downlaodedBytes, int progress) {
                        Log.e("==downLoad==", "Progress" + progress);
                        circleView.setProgress(progress);
                    }
                });
        ThinDownloadManager downloadManager = new ThinDownloadManager();
        downloadManager.add(downloadRequest);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMedia.release();
        unregisterReceiver(mHomeKeyEventReceiver);
    }

    private List<DownloadInfo> localInfos ;
    @Override
    protected void onPause() {
        super.onPause();
        if (mMedia.isPlaying()) {
            mMedia.pause();
        }
        MobclickAgent.onPause(MainActivity.this);
    }

    private boolean isFirstResume = true;
    @Override
    protected void onResume() {
        super.onResume();
        if (!isFirstResume) {
            int sId = MyApplication.getSharedPreferences().getInt(TheConstants.SELECTID, 0);
            DownloadInfo info = FileManager.queryById(MyApplication.getSharedPreferences().getInt(TheConstants.SELECTID, 0));
            Log.e("coming-=-p-=",sId +"****"+defaultId);
            if (info!=null && defaultId != info.getId()) {
                videoNext(info.getUrl());
                tvName.setText(info.getName());
                selectedNum = -1;
                defaultId = info.getId();
            } else if (defaultId!=sId && sId ==0) {
                videoNext("default");
                tvName.setText(getResources().getString(R.string.default_video_name));
                defaultId = 0;
            }else {
                mMedia.start();
            }
            MobclickAgent.onResume(MainActivity.this);
        }else{
            isFirstResume = false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //申请成功，进行相应操作
            Log.e("VERSION6.0", "getGrant");
        } else {
            Log.e("VERSION6.0","notGrant");
            //申请失败，可以继续向用户解释。
            Toast.makeText(this,"未获取到权限无法播放",Toast.LENGTH_SHORT).show();
        }
        return;
    }

    private void placeVIPOrder() {
        String strUserId = MyApplication.getSharedPreferences().getString("UserId", "");
        String strOpenId = MyApplication.getSharedPreferences().getString("wxopenId", "");
        Map<String, String> reqParams = CallRequestUtils.getVipBuyParams(strUserId, strOpenId, 14);

        CallJSONObjectRequest placeOrderRequest = new CallJSONObjectRequest(Request.Method.POST,
                TheConstants.URL_PAY_PREFIX, reqParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsoObject) {
                        int result = JSONUtil.getInt(jsoObject, "result");
                        if(result == 0) {
                            //should not come here
                            Message msg = mHandler.obtainMessage(TheConstants.MSG_PAYMENT_SUCCESS);
                            msg.sendToTarget();
                        } else if (result == 1 || result == 2) {
                            Message msg = mHandler.obtainMessage(TheConstants.MSG_PAYMENT_FAULIRE);
                            msg.sendToTarget();
                        } else if (result == 3) {
                            String strProductOrderId = JSONUtil.getString(jsoObject, "product_order_id");
                            String strTradeOrderId = JSONUtil.getString(jsoObject, "trade_order_id");
                            String strPrepareOrderId = JSONUtil.getString(jsoObject, "prepare_order_id");
                            String strOutTradeNo = JSONUtil.getString(jsoObject, "out_trade_no");
                            String strAppId = JSONUtil.getString(jsoObject, "app_id");
                            String strMchId = JSONUtil.getString(jsoObject, "mch_id");
                            paymentBean = new OrderPaymentStatusBean(
                                    strProductOrderId, strTradeOrderId, strOutTradeNo,
                                    strPrepareOrderId, strAppId, strMchId);
                            Message msg = mHandler.obtainMessage(TheConstants.MSG_PAYMENT_WXPAY_REQUIRED);
                            msg.obj = paymentBean;
                            msg.sendToTarget();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //timeout or server error
                        Message msg = mHandler.obtainMessage(TheConstants.MSG_PAYMENT_TIMEOUT);
                        msg.sendToTarget();
                    }
                });
        MyApplication.getInstance().addToRequestQueue(placeOrderRequest, "");
    }

    /**
     * 监听是否点击了home键将客户端推到后台
     */

    private BroadcastReceiver mHomeKeyEventReceiver = new BroadcastReceiver() {
        String SYSTEM_REASON = "reason";
        String SYSTEM_HOME_KEY = "homekey";
        String SYSTEM_HOME_KEY_LONG = "recentapps";

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)) {
                String reason = intent.getStringExtra(SYSTEM_REASON);
                if (TextUtils.equals(reason, SYSTEM_HOME_KEY)) {
                    //表示按了home键,程序到了后台
                    Log.e("receive","coming");
                    finish();
                }
//                else if(TextUtils.equals(reason, SYSTEM_HOME_KEY_LONG)){
//                    //表示长按home键,显示最近使用的程序列表
//                    finish();
//                }
            }
        }
    };

    /*
    滑动监听
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return detector.onTouchEvent(event);
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        float minMove = 120;         //最小滑动距离
        float minVelocity = 0;      //最小滑动速度
        float beginX = e1.getX();
        float endX = e2.getX();
        float beginY = e1.getY();
        float endY = e2.getY();

        if(beginX-endX>minMove&&Math.abs(velocityX)>minVelocity){   //左滑
//            Toast.makeText(this,velocityX+"左滑",Toast.LENGTH_SHORT).show();
            replaceVideo(TONEXT);
        }else if(endX-beginX>minMove&&Math.abs(velocityX)>minVelocity){   //右滑
//            Toast.makeText(this,velocityX+"右滑",Toast.LENGTH_SHORT).show();
            replaceVideo(TOPREVIOUS);
        }
//        else if(beginY-endY>minMove&&Math.abs(velocityY)>minVelocity){   //上滑
//            Toast.makeText(this,velocityX+"上滑",Toast.LENGTH_SHORT).show();
//        }else if(endY-beginY>minMove&&Math.abs(velocityY)>minVelocity){   //下滑
//            Toast.makeText(this,velocityX+"下滑",Toast.LENGTH_SHORT).show();
//        }
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Uri uri = data.getData();
                Log.e("==test==", data.toString());

               Bitmap bitmap =  getVideoThumbnail(uri.toString());
                Log.e("==test==",(bitmap==null)+"");

            }
        }
    }

    public Bitmap getVideoThumbnail(String filePath) {
        Bitmap bitmap = null;
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        try {
            retriever.setDataSource(filePath);
            bitmap = retriever.getFrameAtTime();
        }
        catch(IllegalArgumentException e) {
            e.printStackTrace();
        }
        catch (RuntimeException e) {
            e.printStackTrace();
        }
        finally {
            try {
                retriever.release();
            }
            catch (RuntimeException e) {
                e.printStackTrace();
            }
        }
        return bitmap;
    }
}
