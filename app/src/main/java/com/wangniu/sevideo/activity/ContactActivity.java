/*
 * Copyright (c) 2015 张涛.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.wangniu.sevideo.activity;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;
import com.wangniu.sevideo.MyApplication;
import com.wangniu.sevideo.R;
import com.wangniu.sevideo.adapter.ContactAdapter;
import com.wangniu.sevideo.bean.Contact;
import com.wangniu.sevideo.db.FileManager;
import com.wangniu.sevideo.util.HanziToPinyin;
import com.wangniu.sevideo.widget.SideBar;

import org.kymjs.kjframe.KJActivity;
import org.kymjs.kjframe.KJHttp;
import org.kymjs.kjframe.ui.BindView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 用户联系人列表
 *
 */
public class ContactActivity extends KJActivity implements SideBar
        .OnTouchingLetterChangedListener, TextWatcher {

    @BindView(id = R.id.school_friend_member)
    private ListView mListView;
    private TextView mFooterView;

    private KJHttp kjh = null;
    private ArrayList<Contact> datas = new ArrayList<>();
    private ContactAdapter mAdapter;

    private TextView tvTittle;
    private ImageButton btnBack;
    private TextView tvSave;
    private String selectedId;

    @Override
    public void setRootView() {
        setContentView(R.layout.activity_contact);
    }

    @Override
    public void initData() {
        super.initData();
        selectedId = getIntent().getStringExtra("VideoId");
//        intent.putExtra("VideoId", String.valueOf(info.getId()));
    }

    @Override
    public void initWidget() {
        super.initWidget();
        SideBar mSideBar = (SideBar) findViewById(R.id.school_friend_sidrbar);
        TextView mDialog = (TextView) findViewById(R.id.school_friend_dialog);

        mSideBar.setTextView(mDialog);
        mSideBar.setOnTouchingLetterChangedListener(this);

        // 给listView设置adapter
        mFooterView = (TextView) View.inflate(aty, R.layout.item_list_contact_count, null);
        mListView.addFooterView(mFooterView);

        tvTittle = (TextView) findViewById(R.id.tv_title);
        tvTittle.setText("通讯录");

        btnBack = (ImageButton) findViewById(R.id.btn_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tvSave = (TextView) findViewById(R.id.tv_save);
        tvSave.setVisibility(View.VISIBLE);
        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MobclickAgent.onEvent(ContactActivity.this, "Ring_15");
                HashMap<Integer, Boolean> isSelected = mAdapter.isSelected;
                ArrayList<Contact> datas = mAdapter.datas;
                ArrayList<Contact> contactsSelect = new ArrayList<Contact>();
                for (int i=0;i<datas.size();i++){
                    if (isSelected.get(i)){
//                        FileManager.updataContactsSId(datas.get(i).getId(),selectedId);
                        Contact contact = datas.get(i);
                        contact.setsId(selectedId);
                        contactsSelect.add(contact);
                    }
                    Log.e("==position==",isSelected.get(i)+""+datas.get(i).getId());
                }
                FileManager.insertContactsSId(contactsSelect);
                finish();
                Toast.makeText(MyApplication.getInstance(),"设置成功",Toast.LENGTH_SHORT).show();
            }
        });
        getData();
    }

    private void getData() {
        List<Contact> contacts = FileManager.queryAllContact();
        if (contacts.size()>0){
            for (int i = 0; i < contacts.size(); i++) {
                Contact data = new Contact();
                data.setName(contacts.get(i).getName());
                data.setId(contacts.get(i).getId());
                data.setNum(contacts.get(i).getNum());
                data.setPinyin(HanziToPinyin.getPinYin(data.getName()));
                datas.add(data);
            }
        }
            mFooterView.setText(datas.size() + "位联系人");
            mAdapter = new ContactAdapter(mListView, datas);
            mListView.setAdapter(mAdapter);

    }

    @Override
    public void onTouchingLetterChanged(String s) {
        int position = 0;
        // 该字母首次出现的位置
        if (mAdapter != null) {
            position = mAdapter.getPositionForSection(s.charAt(0));
        }
        if (position != -1) {
            mListView.setSelection(position);
        } else if (s.contains("#")) {
            mListView.setSelection(0);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        ArrayList<Contact> temp = new ArrayList<>(datas);
        for (Contact data : datas) {
            if (data.getName().contains(s) || data.getPinyin().contains(s)) {
            } else {
                temp.remove(data);
            }
        }
        if (mAdapter != null) {
            mAdapter.refresh(temp);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
