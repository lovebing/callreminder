package com.wangniu.sevideo.activity;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.wangniu.sevideo.R;
import com.wangniu.sevideo.adapter.SpacesItemDownloadDecoration;
import com.wangniu.sevideo.adapter.VideoDowloadAdapter;
import com.wangniu.sevideo.bean.DownloadInfo;
import com.wangniu.sevideo.db.FileManager;

import java.util.List;

/**
 * Created by Administrator on 2016/12/12 0012.
 */
public class DownloadActivity extends BaseActivity implements View.OnClickListener {
    private ImageButton btnBack;
    private TextView tvTitle;
    private RecyclerView rvContent;
    private VideoDowloadAdapter mAdapter;
    private TextView tvReminder;

    @Override
    protected void initVariables() {
        mAdapter = new VideoDowloadAdapter(this);
    }

    @Override
    protected void initViews() {
        setContentView(R.layout.activity_download);
        btnBack = (ImageButton) findViewById(R.id.btn_back);
        btnBack.setOnClickListener(this);
        tvTitle = (TextView) findViewById(R.id.tv_title);
        tvTitle.setText("下载视频");
        rvContent = (RecyclerView) findViewById(R.id.rv_content);
        final GridLayoutManager manager = new GridLayoutManager(this,2);
        rvContent.setLayoutManager(manager);
        rvContent.setAdapter(mAdapter);
        SpacesItemDownloadDecoration itemDecoration = new SpacesItemDownloadDecoration(14);
        rvContent.addItemDecoration(itemDecoration);
        tvReminder = (TextView) findViewById(R.id.tv_reminder);
    }

    @Override
    protected void loadData() {
        List<DownloadInfo> infos = FileManager.getAllInfo();
        if (infos.size()<=0){
            tvReminder.setVisibility(View.VISIBLE);
        }else{
            tvReminder.setVisibility(View.GONE);
        }
        mAdapter.setList(infos);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_back:
                finish();
                break;
        }
    }
}
