package com.wangniu.sevideo.activity;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.dinuscxj.progressbar.CircleProgressBar;
import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListener;
import com.thin.downloadmanager.ThinDownloadManager;
import com.umeng.analytics.MobclickAgent;
import com.wangniu.sevideo.MyApplication;
import com.wangniu.sevideo.R;
import com.wangniu.sevideo.bean.DownloadInfo;
import com.wangniu.sevideo.bean.OrderPaymentStatusBean;
import com.wangniu.sevideo.db.FileManager;
import com.wangniu.sevideo.dialog.GoSettingDialog;
import com.wangniu.sevideo.dialog.PayDialog;
import com.wangniu.sevideo.dialog.QuitDialog;
import com.wangniu.sevideo.dialog.VideoSettingDialog;
import com.wangniu.sevideo.request.CallJSONObjectRequest;
import com.wangniu.sevideo.request.CallRequestUtils;
import com.wangniu.sevideo.util.AndroidUtil;
import com.wangniu.sevideo.util.GoToSettingUtil;
import com.wangniu.sevideo.util.JSONUtil;
import com.wangniu.sevideo.util.PermissionsCheckerUtil;
import com.wangniu.sevideo.util.ShareToTimelineUtil;
import com.wangniu.sevideo.util.TheConstants;
import com.wangniu.sevideo.util.WechatPayUtils;

import org.json.JSONObject;
import org.lovebing.android.oem.Vendor;
import org.lovebing.android.oem.VendorFactory;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Administrator on 2016/12/7 0007.
 */
public class VideoShowActivty extends BaseActivity implements View.OnClickListener {

//    private SurfaceView svShow;
    private ImageButton btnBack;
    private TextView tvTitle;
    private ImageButton btnShare;
    private ImageButton btnBell;
    private ImageButton btnClose;
    private MediaPlayer mMedia;
    private SurfaceHolder mHolder;
    private DownloadInfo info ;
    private RelativeLayout waitProgress;
    private VideoView videoView;
    private SharedPreferences mShare;
    private CircleProgressBar downloadProgress;
    private PayDialog payDialog;
    private OrderPaymentStatusBean paymentBean;
    private String savePath;
    private final int REQUEST_CODE = 0;
    private Bitmap bitmapIcon;
    private Bitmap bitmapCode;
    private final String[] PERMISSIONS = new String[]{
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.PROCESS_OUTGOING_CALLS,
            Manifest.permission.CALL_PHONE,
    };
    private GoSettingDialog toSettingDialog;
    private RelativeLayout rlProgress;
    private TextView tvProgress;
    private VideoSettingDialog mSettingDialog;
    private QuitDialog quitDialog;


    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == PayDialog.TOPAY){
                MobclickAgent.onEvent(VideoShowActivty.this,"cr_pay");
                placeVIPOrder();
            }else if(msg.what == GoSettingDialog.TOSETTINGHOME){
                GoToSettingUtil.goToSetting(VideoShowActivty.this);
            }else if (msg.what == TheConstants.MSG_PAYMENT_WXPAY_REQUIRED){
                WechatPayUtils.pay(paymentBean);
            }else if (msg.what == PayDialog.TOPAY){
                MobclickAgent.onEvent(VideoShowActivty.this,"cr_pay");
                placeVIPOrder();
            }else if (msg.what ==  GoSettingDialog.TOSETTINGWIDOWN){
                Intent intent1 = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent1, REQUEST_CODE);
            }else if (msg.what == VideoSettingDialog.SETVIDEODEFAULT){
                mSettingDialog.dismiss();
                MobclickAgent.onEvent(VideoShowActivty.this, "Ring_13");
                if (info.getId()<0){
                    mShare.edit().putString(TheConstants.RINGSAVEPATH, "default").commit();
                    mShare.edit().putBoolean(TheConstants.SWITCHONOFF, true).commit();
                    Toast.makeText(VideoShowActivty.this, "铃声设置成功", Toast.LENGTH_SHORT).show();
                }else{
                    if (info.getId()>=1008688){
                        mShare.edit().putInt(TheConstants.SELECTID, info.getId()).commit();
                        mShare.edit().putBoolean(TheConstants.SWITCHONOFF, true).commit();
                        mShare.edit().putString(TheConstants.RINGSAVEPATH, "").commit();
                        Toast.makeText(VideoShowActivty.this, "铃声设置成功", Toast.LENGTH_SHORT).show();
                    } else if (!mShare.getBoolean(savePath
                            + info.getId() + ".mp4",false)){
                        mShare.edit().putInt(TheConstants.SELECTID, info.getId()).commit();
                        mShare.edit().putBoolean(TheConstants.SWITCHONOFF, true).commit();
                        mShare.edit().putString(TheConstants.RINGSAVEPATH, "").commit();
                        Toast.makeText(VideoShowActivty.this, "铃声设置成功", Toast.LENGTH_SHORT).show();
                    }else{
                        mShare.edit().putBoolean("setdefault"+info.getId(), true).commit();
                        Toast.makeText(VideoShowActivty.this, "铃声设置正在设置中请稍候......", Toast.LENGTH_SHORT).show();
                        rlProgress.setVisibility(View.VISIBLE);
                    }
                }
            }else if (msg.what == VideoSettingDialog.SETVIDEOFRIEND){
                MobclickAgent.onEvent(VideoShowActivty.this, "Ring_14");
                mSettingDialog.dismiss();
                toContacts();
            }else if (msg.what == QuitDialog.CLICKCONFIRM){
                MobclickAgent.onEvent(VideoShowActivty.this, "Ring_16");
                finish();
            }else if (msg.what == QuitDialog.CLICKCANCLE){
                MobclickAgent.onEvent(VideoShowActivty.this, "Ring_17");
                quitDialog.dismiss();
            }
        }
    };

    private void toContacts(){
        Intent intent = new Intent (this,ContactActivity.class);
        intent.putExtra("VideoId",String.valueOf(info.getId()));
        startActivity(intent);
    }
    @Override
    protected void initVariables() {
        info = (DownloadInfo) getIntent().getSerializableExtra("info");
        mShare = MyApplication.getSharedPreferences();
        savePath = getExternalCacheDir().toString();
        quitDialog = new QuitDialog(this,handler);
    }

    @Override
    protected void initViews() {
        setContentView(R.layout.activity_videoshow);
        btnBack = (ImageButton) findViewById(R.id.btn_back);
        btnBack.setOnClickListener(this);
        tvTitle = (TextView) findViewById(R.id.tv_title);
        btnShare = (ImageButton) findViewById(R.id.btn_share);
        btnShare.setOnClickListener(this);
        btnBell = (ImageButton) findViewById(R.id.btn_bell);
        btnClose = (ImageButton) findViewById(R.id.btn_close);
        btnClose.setOnClickListener(this);
        waitProgress = (RelativeLayout) findViewById(R.id.rl_wait_progress);
//        downloadProgress = (CircleProgressBar) findViewById(R.id.line_progress);
//        downloadProgress.setMax(100);
        tvTitle = (TextView) findViewById(R.id.tv_title);
        if (info!=null){
            tvTitle.setText(info.getName());
        }
        btnBell.setOnClickListener(this);

        rlProgress = (RelativeLayout) findViewById(R.id.rl_progress);
        tvProgress = (TextView) findViewById(R.id.tv_progress);
        //intVideo
        initVideo();

    }

    Uri mUri;
    private void initVideo() {
        videoView = (VideoView) findViewById(R.id.videoView);
        MediaController controller  = new MediaController(this);
        videoView.setMediaController(controller);
        controller.setVisibility(View.GONE);
        if (info!=null){
            DownloadInfo info_new = FileManager.queryById(info.getId());
            if(info_new!=null){
                mUri = Uri.parse(info_new.getUrl());
                videoView.setVideoURI(mUri);
                info =null;
                info = info_new;
            }else if (info.getId()<0){
//                AssetFileDescriptor afd = null;
//                try {
//                    afd = getAssets().openFd("test.mp4");
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                String uri = "android.resource://" + getPackageName() + "/" + (R.raw.default);
                mUri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.dvideo);
                videoView.setVideoURI(mUri);
            }
            else{
                mUri = Uri.parse(info.getUrl());
                videoView.setVideoURI(mUri);
            }
        }
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                waitProgress.setVisibility(View.GONE);
                videoView.start();
            }
        });
        videoView.setOnInfoListener(new MediaPlayer.OnInfoListener() {
            @Override
            public boolean onInfo(MediaPlayer mp, int what, int extra) {
                switch (what) {
                    case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                        waitProgress.setVisibility(View.VISIBLE);
                        break;
                    case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                        waitProgress.setVisibility(View.GONE);
                        break;
                }
                return false;
            }
        });
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                videoView.setVideoURI(mUri);
            }
        });
    }


    @Override
    protected void loadData() {

    }



    @Override
    protected void onPause() {
        super.onPause();
        if (videoView.isPlaying()) {
            videoView.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        videoView.resume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

//    private void dowloadVideo(String downloadstr, final String destinationstr) {
//        File file = new File(destinationstr);
//        if (file.exists()) {
//            if (mShare.getBoolean(destinationstr,false)){
//                mShare.edit().putInt(TheConstants.SELECTID, info.getId()).commit();
//                mShare.edit().putBoolean(TheConstants.SWITCHONOFF, true).commit();
//                mShare.edit().putString(TheConstants.RINGSAVEPATH, "").commit();
//                Toast.makeText(VideoShowActivty.this, "铃声设置成功", Toast.LENGTH_SHORT).show();
//            }else{
////                downloadProgress.setVisibility(View.VISIBLE);
//                rlProgress.setVisibility(View.VISIBLE);
//            }
//            return;
//        }
//        //付费逻辑
////        if (mShare.getInt(TheConstants.SETTIME,-1)==1){
////            if (payDialog == null){
////                payDialog = new PayDialog(this,handler);
////            }
////            payDialog.show();
////            return;
////        }
//        Uri downloadUri = Uri.parse(downloadstr);
//        Uri destinationUri = Uri.parse(destinationstr);//this.getExternalCacheDir().toString()+"/test.mp4"
////        downloadProgress.setVisibility(View.VISIBLE);
//        rlProgress.setVisibility(View.VISIBLE);
//        waitProgress.setVisibility(View.GONE);
//        DownloadRequest downloadRequest = new DownloadRequest(downloadUri)
////                .addCustomHeader("Auth-Token", "YourTokenApiKey")
//                .setRetryPolicy(new DefaultRetryPolicy())
//                .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.HIGH)
//                .setDownloadContext(this)//Optional
//                .setDownloadListener(new DownloadStatusListener() {
//                    @Override
//                    public void onDownloadComplete(int id) {
//                        Log.e("==downLoad==", "complete");
////                        downloadProgress.setVisibility(View.GONE);
//                        rlProgress.setVisibility(View.GONE);
//                        //存储本地视频信息
//                        DownloadInfo info_new = new DownloadInfo();
//                        info_new.setId(info.getId());
//                        info_new.setName(info.getName());
//                        Log.e("downloadinfo",info.getImg());
//                        info_new.setImg(info.getImg());
//                        info_new.setUrl(destinationstr);
//                        FileManager.addInfo(info_new);
////                        mShare.edit().putString(TheConstants.RINGSAVEPATH, destinationstr).commit();
////                        mShare.edit().putString(TheConstants.RINGSAVENAME, downloadInfos.get(selectedNum).getName()).commit();
//                        mShare.edit().putInt(TheConstants.SELECTID, info.getId()).commit();
//                        mShare.edit().putBoolean(TheConstants.SWITCHONOFF, true).commit();
//                        mShare.edit().putString(TheConstants.RINGSAVEPATH, "").commit();
//                        mShare.edit().putInt(TheConstants.SETTIME, mShare.getInt(TheConstants.SETTIME, -1) + 1).commit();
//                        mShare.edit().putBoolean(destinationstr, true).commit();
//                        Toast.makeText(VideoShowActivty.this, "铃声设置成功", Toast.LENGTH_SHORT).show();
//                    }
//
//                    @Override
//                    public void onDownloadFailed(int id, int errorCode, String errorMessage) {
//                        Log.e("==downLoad==", "Failed" + errorMessage.toString());
//                    }
//
//
//                    @Override
//                    public void onProgress(int id, long totalBytes, long downlaodedBytes, int progress) {
//                        Log.e("==downLoad==", "Progress" + progress);
////                        downloadProgress.setProgress(progress);
//                        tvProgress.setText(progress+"%");
//                    }
//                });
//        ThinDownloadManager downloadManager = new ThinDownloadManager();
//        downloadManager.add(downloadRequest);
//    }

    private void dowloadVideo(String downloadstr, final String destinationstr) {
        File file = new File(destinationstr);
        if (info.getId()<0){
            return;
        }

        if (file.exists()) {
            if (!mShare.getBoolean(destinationstr,false)){
//                mShare.edit().putInt(TheConstants.SELECTID, info.getId()).commit();
//                mShare.edit().putBoolean(TheConstants.SWITCHONOFF, true).commit();
//                mShare.edit().putString(TheConstants.RINGSAVEPATH, "").commit();
//                Toast.makeText(VideoShowActivty.this, "铃声设置成功", Toast.LENGTH_SHORT).show();
            }else{
                rlProgress.setVisibility(View.VISIBLE);
            }
            return;
        }
        Log.e("SetTime",mShare.getInt(TheConstants.SETTIME,-1)+"***");
        if (mShare.getInt(TheConstants.SETTIME,-1)==1){
            if (payDialog == null){
                payDialog = new PayDialog(this,handler);
            }
            payDialog.show();
            mSettingDialog.dismiss();
            return;
        }
        Uri downloadUri = Uri.parse(downloadstr);
        Uri destinationUri = Uri.parse(destinationstr);//this.getExternalCacheDir().toString()+"/test.mp4"
        rlProgress.setVisibility(View.VISIBLE);
        waitProgress.setVisibility(View.GONE);
        DownloadRequest downloadRequest = new DownloadRequest(downloadUri)
                .setRetryPolicy(new DefaultRetryPolicy())
                .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.HIGH)
                .setDownloadContext(this)//Optional
                .setDownloadListener(new DownloadStatusListener() {
                    @Override
                    public void onDownloadComplete(int id) {
                        Log.e("==downLoad==", "complete");
                        mShare.edit().putInt(TheConstants.SETTIME, mShare.getInt(TheConstants.SETTIME, -1) + 1).commit();
                        rlProgress.setVisibility(View.GONE);
                        //存储本地视频信息
                        DownloadInfo info_new = new DownloadInfo();
                        info_new.setId(info.getId());
                        info_new.setName(info.getName());
                        Log.e("downloadinfo", info.getImg());
                        info_new.setImg(info.getImg());
                        info_new.setUrl(destinationstr);
                        FileManager.addInfo(info_new);
//                        mShare.edit().putInt(TheConstants.SELECTID, info.getId()).commit();
//                        mShare.edit().putBoolean(TheConstants.SWITCHONOFF, true).commit();
//                        mShare.edit().putString(TheConstants.RINGSAVEPATH, "").commit();
//                        mShare.edit().putInt(TheConstants.SETTIME, mShare.getInt(TheConstants.SETTIME, -1) + 1).commit();
                        mShare.edit().putBoolean(destinationstr, false).commit();
                        if (mShare.getBoolean("setdefault"+info.getId(), false)){
                            mShare.edit().putInt(TheConstants.SELECTID, info.getId()).commit();
                            mShare.edit().putBoolean(TheConstants.SWITCHONOFF, true).commit();
                            mShare.edit().putString(TheConstants.RINGSAVEPATH, "").commit();
                            Toast.makeText(VideoShowActivty.this, "铃声设置成功", Toast.LENGTH_SHORT).show();
                        };
//                        Toast.makeText(VideoShowActivty.this, "铃声设置成功", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onDownloadFailed(int id, int errorCode, String errorMessage) {
                        mShare.edit().putBoolean(destinationstr, false).commit();
                    }

                    @Override
                    public void onProgress(int id, long totalBytes, long downlaodedBytes, int progress) {
                        mShare.edit().putBoolean(destinationstr, true).commit();
                        tvProgress.setText(progress+"%");
                    }
                });
        ThinDownloadManager downloadManager = new ThinDownloadManager();
        downloadManager.add(downloadRequest);
    }

    private void placeVIPOrder() {
        String strUserId = MyApplication.getSharedPreferences().getString("UserId", "");
        String strOpenId = MyApplication.getSharedPreferences().getString("wxopenId", "");
        Map<String, String> reqParams = CallRequestUtils.getVipBuyParams(strUserId, strOpenId, 14);

        CallJSONObjectRequest placeOrderRequest = new CallJSONObjectRequest(Request.Method.POST,
                TheConstants.URL_PAY_PREFIX, reqParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsoObject) {
                        int result = JSONUtil.getInt(jsoObject, "result");
                        if(result == 0) {
                            //should not come here
                            Message msg = handler.obtainMessage(TheConstants.MSG_PAYMENT_SUCCESS);
                            msg.sendToTarget();
                        } else if (result == 1 || result == 2) {
                            Message msg = handler.obtainMessage(TheConstants.MSG_PAYMENT_FAULIRE);
                            msg.sendToTarget();
                        } else if (result == 3) {
                            String strProductOrderId = JSONUtil.getString(jsoObject, "product_order_id");
                            String strTradeOrderId = JSONUtil.getString(jsoObject, "trade_order_id");
                            String strPrepareOrderId = JSONUtil.getString(jsoObject, "prepare_order_id");
                            String strOutTradeNo = JSONUtil.getString(jsoObject, "out_trade_no");
                            String strAppId = JSONUtil.getString(jsoObject, "app_id");
                            String strMchId = JSONUtil.getString(jsoObject, "mch_id");
                            paymentBean = new OrderPaymentStatusBean(
                                    strProductOrderId, strTradeOrderId, strOutTradeNo,
                                    strPrepareOrderId, strAppId, strMchId);
                            Message msg = handler.obtainMessage(TheConstants.MSG_PAYMENT_WXPAY_REQUIRED);
                            msg.obj = paymentBean;
                            msg.sendToTarget();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //timeout or server error
                        Message msg = handler.obtainMessage(TheConstants.MSG_PAYMENT_TIMEOUT);
                        msg.sendToTarget();
                    }
                });
        MyApplication.getInstance().addToRequestQueue(placeOrderRequest, "");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_bell:
                MobclickAgent.onEvent(VideoShowActivty.this, "Ring_11");
                Vendor vendor = VendorFactory.getVendor(VideoShowActivty.this);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (PermissionsCheckerUtil.lacksPermissions(PERMISSIONS, this)){
                        if (toSettingDialog==null){
                            toSettingDialog = new GoSettingDialog(VideoShowActivty.this,handler, 0);
                        }
                        toSettingDialog.show();
                    }else if (!Settings.canDrawOverlays(VideoShowActivty.this)){
                        toSettingDialog = new GoSettingDialog(VideoShowActivty.this,handler,1);
                        toSettingDialog.show();
                    } else if (!vendor.notificationListenersEnabled() || !vendor.getName().equals(Vendor.Name.GENERAL)) {
                        toSettingDialog = new GoSettingDialog(VideoShowActivty.this,handler, 2);
                        toSettingDialog.show();
                    }
                }
                    if (mSettingDialog == null) {
                        mSettingDialog = new VideoSettingDialog(this, handler);
                    }
                    mSettingDialog.show();
                if (info.getId()>=1008688){
                    info.setImg("");
                    FileManager.addInfo(info);
                }else if (info.getId()<0){
                    mShare.edit().putString(TheConstants.RINGSAVEPATH, "default").commit();
                    mShare.edit().putInt(TheConstants.SELECTID, 0).commit();
                    mShare.edit().putBoolean(TheConstants.SWITCHONOFF, true).commit();
                }
                else if (!mShare.getBoolean(savePath
                        + info.getId() + ".mp4",false)){
                    dowloadVideo(info.getUrl(), savePath
                                + info.getId() + ".mp4");
                }
//                if (info.getId()<0){
//                    mShare.edit().putString(TheConstants.RINGSAVEPATH, "default").commit();
//                    mShare.edit().putBoolean(TheConstants.SWITCHONOFF, true).commit();
//                    Toast.makeText(VideoShowActivty.this, "铃声设置成功", Toast.LENGTH_SHORT).show();
//                }else{
//                    dowloadVideo(info.getUrl(), savePath
//                            + info.getId() + ".mp4");
//                }
                break;
            case R.id.btn_back:
                clickClose();
                break;
            case R.id.btn_close:
                MobclickAgent.onEvent(MyApplication.getInstance(), "Ring_04");
                clickClose();
                break;
            case R.id.btn_share:
                MobclickAgent.onEvent(VideoShowActivty.this, "Ring_12");
                if (bitmapIcon==null){
                    bitmapIcon = BitmapFactory.decodeResource(getResources(),R.mipmap.img_share_icon);
                }
                if (bitmapCode==null){
                    bitmapCode = BitmapFactory.decodeResource(getResources(),R.mipmap.img_share_code);
                }
                if (AndroidUtil.isWeixinAvilible(this)){
                    ShareToTimelineUtil.shareToTimeline("一键设置来电铃声视频秀\n"+
                            "传统来电铃声单调、乏味，视频铃声应用让您的来电铃声看得见\n",
                            "http://hetongbu.net/?i=ml6h",bitmapIcon,bitmapCode,null,this);
                }
                break;
        }
    }
    private void clickClose(){
        if (mShare.getBoolean(savePath
                + info.getId() + ".mp4",false)){
            quitDialog.show();
        }else{
            finish();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            clickClose();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
