package com.wangniu.sevideo.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.sdk.android.feedback.impl.FeedbackAPI;
import com.umeng.analytics.MobclickAgent;
import com.wangniu.sevideo.MyApplication;
import com.wangniu.sevideo.R;
import com.wangniu.sevideo.bean.Contact;
import com.wangniu.sevideo.bean.DownloadInfo;
import com.wangniu.sevideo.db.FileManager;
import com.wangniu.sevideo.dialog.PraiseDialog;
import com.wangniu.sevideo.util.TheConstants;

import java.util.List;

/**
 * Created by Administrator on 2016/11/16 0016.
 */
public class SettingActivity extends Activity implements View.OnClickListener {

    private ImageButton btnBack;
    private SwitchCompat switchCompat;
    private SharedPreferences mShare;
    private TextView tvName;
    private RelativeLayout rlPraise;
    private RelativeLayout rlFeedback;
    private TextView tvDownloadCount;
    private RelativeLayout rlDownload;
    private PraiseDialog mPraiseDialog;
    private AppCompatSeekBar mSeekBar;
    private TextView tvPrecent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        mShare = MyApplication.getSharedPreferences();
        List<Contact> contacts = FileManager.queryAllContactSelect();
        for (Contact contact : contacts){
            Log.e("==AllCon=", contact.toString());
        }
        initView();
    }

    private void initView() {
        btnBack = (ImageButton) findViewById(R.id.btn_back);
        btnBack.setOnClickListener(this);
        switchCompat = (SwitchCompat) findViewById(R.id.switch_onoff);
        switchCompat.setOnClickListener(this);
        switchCompat.setChecked(mShare.getBoolean(TheConstants.SWITCHONOFF, false));
        tvName = (TextView) findViewById(R.id.tv_saveName);
        if ( "default".equals(MyApplication.getSharedPreferences().getString(TheConstants.RINGSAVEPATH, ""))){
            tvName.setText(getResources().getString(R.string.default_video_name));
        }else{
            DownloadInfo downloadInfo = FileManager.queryById(mShare.getInt(TheConstants.SELECTID,0 ));
            if (downloadInfo!=null){
                tvName.setText(downloadInfo.getName());
            }
        }
        rlPraise = (RelativeLayout) findViewById(R.id.rl_praise);
        rlPraise.setOnClickListener(this);
        rlFeedback = (RelativeLayout) findViewById(R.id.rl_feedback);
        rlFeedback.setOnClickListener(this);
        tvDownloadCount = (TextView) findViewById(R.id.tv_download_count);
        tvDownloadCount.setText(FileManager.getAllInfo().size()+"个");;
        rlDownload = (RelativeLayout) findViewById(R.id.rl_download);
        rlDownload.setOnClickListener(this);

        int precent = mShare.getInt("voice_precent",70);
        tvPrecent = (TextView) findViewById(R.id.tv_precent);
        tvPrecent.setText(precent+"%");
        mSeekBar = (AppCompatSeekBar) findViewById(R.id.voice_percent);
        mSeekBar.setMax(100);
        mSeekBar.setProgress(precent);
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tvPrecent.setText(progress+"%");
                mShare.edit().putInt("voice_precent",progress).commit();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_back:
                finish();
                break;
            case R.id.switch_onoff:
                boolean onoff = switchCompat.isChecked();
                if (onoff==true){
                    Toast.makeText(SettingActivity.this,"来电视频开启",Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(SettingActivity.this,"来电视频关闭",Toast.LENGTH_SHORT).show();
                }
                mShare.edit().putBoolean(TheConstants.SWITCHONOFF, onoff).commit();
                break;
            case R.id.rl_praise:
                if (mPraiseDialog == null){
                    mPraiseDialog = new PraiseDialog(this,1);
                }
                mPraiseDialog.show();
//                Intent intent11 = new Intent(this,ContactActivity.class);
//                startActivity(intent11);
                break;
            case R.id.rl_feedback:
                MobclickAgent.onEvent(MyApplication.getInstance(), "Ring_06");
                FeedbackAPI.openFeedbackActivity();
                break;
            case R.id.rl_download:
                MobclickAgent.onEvent(MyApplication.getInstance(), "Ring_05");
                Intent intent = new Intent(this,DownloadActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
