package com.wangniu.sevideo.adapter;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;

/**
 * Created by Administrator on 2016/11/28 0028.
 */
public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private int spacing;
//    private boolean isRtl = TextUtilsCompat.getLayoutDirectionFromLocale(Locale.getDefault()) == ViewCompat.LAYOUT_DIRECTION_RTL;

    public SpacesItemDecoration( int spacing) {
        this.spacing = spacing;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);
        if (position!=0){
            outRect.left = spacing;
            outRect.right = spacing;
            outRect.bottom = spacing*2;
        }
    }
}
