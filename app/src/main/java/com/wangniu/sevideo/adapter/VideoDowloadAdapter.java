package com.wangniu.sevideo.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wangniu.sevideo.R;
import com.wangniu.sevideo.activity.VideoShowActivty;
import com.wangniu.sevideo.bean.DownloadInfo;
import com.wangniu.sevideo.util.MeasureUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/12/7 0007.
 */
public class VideoDowloadAdapter extends RecyclerView.Adapter {

    private List<DownloadInfo> infos = new ArrayList<>();
    private Context mContext;
    private int windownWidth;

    public VideoDowloadAdapter(Context context) {
        mContext = context;
        windownWidth = MeasureUtil.getWidownSize(context).get(0);
    }

    public void setList(List<DownloadInfo> infos){
        if (infos != null){
            this.infos = infos;
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_vd,parent,false);
        return new ItemHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ItemHolder holder1 = (ItemHolder) holder;
        holder1.tvTitle.setText(infos.get(position).getName());
        int imgWidth = windownWidth/2;
        if (infos.get(position).getId()<0){
            Picasso.with(mContext).load(R.mipmap.img_video_default).resize(imgWidth,imgWidth*17/25).into(holder1.ivConent);
        }else{
            Picasso.with(mContext).load(infos.get(position).getImg()).resize(imgWidth,imgWidth*17/25).into(holder1.ivConent);
        }
        holder1.contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,VideoShowActivty .class);
                intent.putExtra("info",infos.get(position));
                mContext.startActivity(intent);
                Log.e("==coming==","adapter");
            }
        });
    }



    @Override
    public int getItemCount() {
        return infos.size();
    }

    public static class ItemHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle;
        private View contentView;
        private ImageView ivConent;
        private TextView tvCurrent;
        public ItemHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            contentView = itemView;
            ivConent = (ImageView) itemView.findViewById(R.id.iv_content);
            tvCurrent = (TextView) itemView.findViewById(R.id.tv_current);
        }
    }
}
