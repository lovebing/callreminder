
package com.wangniu.sevideo.adapter;

import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.wangniu.sevideo.R;
import com.wangniu.sevideo.bean.Contact;

import org.kymjs.kjframe.KJBitmap;
import org.kymjs.kjframe.widget.AdapterHolder;
import org.kymjs.kjframe.widget.KJAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * 列表适配器
 *
 * @author kymjs (http://www.kymjs.com/) on 9/16/15.
 */
public class ContactAdapter extends KJAdapter<Contact> implements SectionIndexer {

    private KJBitmap kjb = new KJBitmap();
    public ArrayList<Contact> datas;
    public static HashMap<Integer, Boolean> isSelected;

    public ContactAdapter(AbsListView view, ArrayList<Contact> mDatas) {
        super(view, mDatas, R.layout.item_list_contact);
        datas = mDatas;
        if (datas == null) {
            datas = new ArrayList<>();
        }
        Collections.sort(datas);

        init();
    }

    public void init() {
        isSelected = new HashMap<Integer, Boolean>();
        for (int i = 0; i < datas.size(); i++) {
            isSelected.put(i, false);
        }
    }

    @Override
    public void convert(AdapterHolder helper, Contact item, boolean isScrolling) {
    }

    @Override
    public void convert(AdapterHolder holder, Contact item, final boolean isScrolling, final int position) {

        holder.setText(R.id.contact_title, item.getName());

        TextView tvLetter = holder.getView(R.id.contact_catalog);
//        TextView tvLine = holder.getView(R.id.contact_line);
        TextView tvnum = holder.getView(R.id.contact_num);
        tvnum.setText(datas.get(position).getNum());
        final CheckBox cbCheck = holder.getView(R.id.cb_selected);
        if (isSelected.get(position)){
            cbCheck.setChecked(true);
        }else{
            cbCheck.setChecked(false);
        }
        cbCheck.setTag(position);
        cbCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.e("onChecked", isChecked + "");
                int position = (int) buttonView.getTag();
                isSelected.put(position, isChecked);
            }
        });

        LinearLayout llMain = holder.getView(R.id.ll_main);
        llMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cbCheck.setChecked(!isSelected.get(position));
            }
        });

        //如果是第0个那么一定显示#号
        if (position == 0) {
            tvLetter.setVisibility(View.VISIBLE);
            tvLetter.setText("#");
//            tvLine.setVisibility(View.VISIBLE);
        } else {

            //如果和上一个item的首字母不同，则认为是新分类的开始
            Contact prevData = datas.get(position - 1);
            if (item.getFirstChar() != prevData.getFirstChar()) {
                tvLetter.setVisibility(View.VISIBLE);
                tvLetter.setText("" + item.getFirstChar());
//                tvLine.setVisibility(View.VISIBLE);
            } else {
                tvLetter.setVisibility(View.GONE);
//                tvLine.setVisibility(View.GONE);
            }
        }
    }

    /**
     * 根据ListView的当前位置获取分类的首字母的Char ascii值
     */
    public int getSectionForPosition(int position) {
        Contact item = datas.get(position);
        return item.getFirstChar();
    }

    /**
     * 根据分类的首字母的Char ascii值获取其第一次出现该首字母的位置
     */
    public int getPositionForSection(int section) {
        for (int i = 0; i < getCount(); i++) {
            char firstChar = datas.get(i).getFirstChar();
            if (firstChar == section) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public Object[] getSections() {
        return null;
    }

}
