package com.wangniu.sevideo.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.umeng.analytics.MobclickAgent;
import com.wangniu.sevideo.MyApplication;
import com.wangniu.sevideo.R;
import com.wangniu.sevideo.activity.VideoShowActivty;
import com.wangniu.sevideo.bean.DownloadInfo;
import com.wangniu.sevideo.bean.VideoBitmap;
import com.wangniu.sevideo.bean.VideoInfo;
import com.wangniu.sevideo.util.GetLocalVideoImg;
import com.wangniu.sevideo.util.MeasureUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2016/12/7 0007.
 */
public class VideoLocalAdapter extends RecyclerView.Adapter {

    private List<VideoInfo> infos = new ArrayList<>();
    private Context mContext;
    private int windownWidth;

    public VideoLocalAdapter(Context context) {
        mContext = context;
        windownWidth = MeasureUtil.getWidownSize(context).get(0);
    }

    public void setList(List<VideoInfo> infos){
        if (infos != null){
            this.infos = infos;
        }
    }

    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            VideoBitmap videoBitmap = (VideoBitmap) msg.obj;
            videoBitmap.getImageView().setBackgroundDrawable(new BitmapDrawable(videoBitmap.getBitmap()));
        }
    };


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_vd,parent,false);
        return new ItemHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ItemHolder holder1 = (ItemHolder) holder;
        holder1.tvTitle.setText(infos.get(position).getDisplayName());
        int imgWidth = windownWidth/2;
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder1.ivConent.getLayoutParams();
        params.width = imgWidth;
        params.height = imgWidth*68/100;
        holder1.ivConent.setLayoutParams(params);
        if (infos.get(position).getImage()!=null){
//            holder1.ivConent.setImageBitmap(infos.get(position).getImage());
//            holder1.ivConent.setBackgroundDrawable(new BitmapDrawable(infos.get(position).getImage()));
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                Bitmap bm= ThumbnailUtils.createVideoThumbnail(infos.get(position).getPath(), MediaStore.Video.Thumbnails.MINI_KIND);
                VideoBitmap videoBitmap = new VideoBitmap(holder1.ivConent,bm);
                Message msg = Message.obtain();
                msg.what = 100;
                msg.obj = videoBitmap;
                handler.sendMessage(msg);
//                holder1.ivConent.setBackgroundDrawable(new BitmapDrawable(bm));
            }
        }).start();

//        Picasso.with(mContext).load(infos.get(position).getImgUrl()).resize(imgWidth,imgWidth*17/25).into(holder1.ivConent);
        holder1.contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MobclickAgent.onEvent(MyApplication.getInstance(), "Ring_02");
                DownloadInfo downloadInfo = new DownloadInfo();
                downloadInfo.setName(infos.get(position).getDisplayName());
                downloadInfo.setUrl(infos.get(position).getPath());
                downloadInfo.setId(infos.get(position).getId()+1008688);
                Intent intent = new Intent(mContext,VideoShowActivty .class);
                intent.putExtra("info",downloadInfo);
                mContext.startActivity(intent);
                Log.e("==coming==", "adapter");
            }
        });
    }



    @Override
    public int getItemCount() {
        return infos.size();
    }

    public static class ItemHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle;
        private View contentView;
        private ImageView ivConent;
        private TextView tvCurrent;
        public ItemHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            contentView = itemView;
            ivConent = (ImageView) itemView.findViewById(R.id.iv_content);
            tvCurrent = (TextView) itemView.findViewById(R.id.tv_current);
        }
    }
}
