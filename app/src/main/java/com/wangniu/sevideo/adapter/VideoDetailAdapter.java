package com.wangniu.sevideo.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.umeng.analytics.MobclickAgent;
import com.wangniu.sevideo.MyApplication;
import com.wangniu.sevideo.R;
import com.wangniu.sevideo.activity.VideoShowActivty;
import com.wangniu.sevideo.bean.DownloadInfo;
import com.wangniu.sevideo.db.FileManager;
import com.wangniu.sevideo.util.MeasureUtil;
import com.wangniu.sevideo.util.TheConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/12/7 0007.
 */
public class VideoDetailAdapter extends RecyclerView.Adapter {

    private static final int ITEM_VIEW_TYPE_HEADER = 0;
    private static final int ITEM_VIEW_TYPE_ITEM = 1;
    private List<DownloadInfo> infos = new ArrayList<>();
    private Context mContext;
    private int windownWidth;

    public VideoDetailAdapter(Context context) {
        mContext = context;
        windownWidth = MeasureUtil.getWidownSize(context).get(0);
    }

    public boolean isHeader(int position){
        return position == 0;
    }

    public void setList(List<DownloadInfo> infos){
        if (infos != null){
            this.infos = infos;
        }
        DownloadInfo info = new DownloadInfo();
        info.setId(-10086);
        info.setName(mContext.getResources().getString(R.string.default_video_name));
        infos.add(info);
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_VIEW_TYPE_HEADER){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_vd_header,parent,false);
            return new ItemHolder(view);
        }else{
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_vd,parent,false);
            return new ItemHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final DownloadInfo info = FileManager.queryById(MyApplication.getSharedPreferences().getInt(TheConstants.SELECTID, 0));
        if (isHeader(position)){
            ItemHolder holder1 = (ItemHolder) holder;
            if ("default".equals(MyApplication.getSharedPreferences().getString(TheConstants.RINGSAVEPATH, ""))){
                holder1.tvTitle.setText(mContext.getResources().getString(R.string.default_video_name));
                Picasso.with(mContext).load(R.mipmap.img_video_default).placeholder(R.mipmap.img_video_place).resize(windownWidth, windownWidth * 17 / 25).into(holder1.ivConent);
                holder1.contentView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, VideoShowActivty.class);
                        DownloadInfo info_default = new DownloadInfo();
                        info_default.setId(-10086);
                        info_default.setName(mContext.getResources().getString(R.string.default_video_name));
                        intent.putExtra("info", info_default);
                        mContext.startActivity(intent);
                        Log.e("==coming==", "adapter");
                    }
                });
            }else if (info!=null&&info.getId()<1008688){
                holder1.tvTitle.setText(info.getName());
                if(info.getImg()!=null && !"".equals(info.getImg())){
                    Picasso.with(mContext).load(info.getImg()).placeholder(R.mipmap.img_video_place).resize(windownWidth, windownWidth * 17 / 25).into(holder1.ivConent);
                }
                holder1.contentView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, VideoShowActivty.class);
                        intent.putExtra("info", info);
                        mContext.startActivity(intent);
                        Log.e("==coming==", "adapter");
                    }
                });
            }else{
                holder1.tvTitle.setText(mContext.getResources().getString(R.string.default_video_name));
                Picasso.with(mContext).load(R.mipmap.img_video_default).resize(windownWidth, windownWidth*17/25).into(holder1.ivConent);

                holder1.contentView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, VideoShowActivty.class);
                        DownloadInfo info_default = new DownloadInfo();
                        info_default.setId(-10086);
                        info_default.setName(mContext.getResources().getString(R.string.default_video_name));
                        intent.putExtra("info", info_default);
                        mContext.startActivity(intent);
                        Log.e("==coming==", "adapter");
                    }
                });
            }
            return;
        }
        ItemHolder holder1 = (ItemHolder) holder;
        holder1.tvTitle.setText(infos.get(position - 1).getName());
        int imgWidth = windownWidth/2;
        if (infos.get(position-1).getId()<0){
            Picasso.with(mContext).load(R.mipmap.img_video_default).resize(imgWidth,imgWidth*17/25).into(holder1.ivConent);
        }else{
            Picasso.with(mContext).load(infos.get(position - 1).getImg()).resize(imgWidth,imgWidth*17/25).into(holder1.ivConent);
        }
        holder1.contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MobclickAgent.onEvent(MyApplication.getInstance(), "Ring_10");
                Intent intent = new Intent(mContext,VideoShowActivty .class);
                intent.putExtra("info",infos.get(position-1));
                mContext.startActivity(intent);
                Log.e("==coming==","adapter");
            }
        });
        if ("default".equals(MyApplication.getSharedPreferences().getString(TheConstants.RINGSAVEPATH, ""))){
            holder1.tvCurrent.setVisibility(View.GONE);
        } else if (info!=null && info.getId()==infos.get(position-1).getId()){
            holder1.tvCurrent.setVisibility(View.VISIBLE);
        }else{
            holder1.tvCurrent.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return isHeader(position) ? ITEM_VIEW_TYPE_HEADER : ITEM_VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return infos.size()+1;
    }

    public static class ItemHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle;
        private View contentView;
        private ImageView ivConent;
        private TextView tvCurrent;
        public ItemHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            contentView = itemView;
            ivConent = (ImageView) itemView.findViewById(R.id.iv_content);
            tvCurrent = (TextView) itemView.findViewById(R.id.tv_current);
        }
    }
}
