package com.wangniu.sevideo.reciver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.wangniu.sevideo.service.KeepLiveService;

/**
 * Created by lovebing on 2017/12/3.
 */
public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(getClass().getSimpleName(), "action=" + intent.getAction());
        if(intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            Intent target = new Intent(context, KeepLiveService.class);
            context.startService(target);
        }
    }
}
