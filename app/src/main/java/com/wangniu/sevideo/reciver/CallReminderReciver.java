package com.wangniu.sevideo.reciver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.media.AudioManager;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.wangniu.sevideo.MyApplication;
import com.wangniu.sevideo.activity.AcceptCallActivity;
import com.wangniu.sevideo.util.CallUtils;
import com.wangniu.sevideo.util.MeasureUtil;
import com.wangniu.sevideo.util.TheConstants;
import com.wangniu.sevideo.util.VolumeTool;
import com.wangniu.sevideo.widget.DeskLayout;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2016/11/14 0014.
 */
public class CallReminderReciver extends BroadcastReceiver {

    private final String TAG = "CallReminder";
    private TelephonyManager telephony;
    private Context mContext;
    private Intent mIntent;
    private static int currentVolum = 0;
    private static AudioManager audioManager;
    private static DeskLayout deskLayout;
    private static WindowManager manager;
    private static boolean endClick = false;
    private static boolean IsRing = false;
    WindowManager.LayoutParams mLayout;
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == DeskLayout.ANSWERCALL) {
                if (telephony != null) {
//                    CallUtils.answerPhoneHeadsethook(mContext, mIntent);
                    acceptCall(mContext);
                    closeWindow();
                    endClick = true;
                }
            } else if (msg.what == DeskLayout.ENDCALL) {
                CallUtils.disconnectPhoneItelephony(mContext);
                closeWindow();
                endClick = true;
            }
        }
    };

    private void closeWindow() {
        if (IsRing){
            Log.e("manager==", (manager == null) + "***" + (audioManager == null));
//        deskLayout.release();
            manager.removeView(deskLayout);
//        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentVolum, 0);
        }
        IsRing = false;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
//        Log.i(TAG, "[Broadcast]" + action);
        mContext = context;
        Log.e(TAG, "[Broadcast]" + action);
        //呼入电话
        if (action.equals(Intent.ACTION_NEW_OUTGOING_CALL)) {
            Log.i(TAG, "[Broadcast]PHONE_STATE");
            doReceivePhone(context, intent);
        } else if (action.equals("android.intent.action.PHONE_STATE")) {
            doReceivePhone(context, intent);
        }
    }

    /**
     * 处理电话广播.
     *
     * @param context
     * @param intent
     */
    public void doReceivePhone(Context context, Intent intent) {
        String phoneNumber = intent.getStringExtra(
                TelephonyManager.EXTRA_INCOMING_NUMBER);
        telephony =
                (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        int state = telephony.getCallState();
        switch (state) {
            case TelephonyManager.CALL_STATE_RINGING:
                IsRing =true;
                Log.e("ring==","coming"+MyApplication.getSharedPreferences().getBoolean(TheConstants.SWITCHONOFF,true));
            if (MyApplication.getSharedPreferences().getBoolean(TheConstants.SWITCHONOFF,true)){
//                VolumeTool.setVolume(context, 0);
                checkPhoneNum(context, phoneNumber);
                Log.i(TAG, "[Broadcast]等待接电话=" + phoneNumber);
                mIntent = null;
                mIntent = intent;
                }
                break;
            case TelephonyManager.CALL_STATE_IDLE:
                Log.i(TAG, "[Broadcast]电话挂断=" + phoneNumber);
                Log.e("jiojo", endClick+"**");
                if (MyApplication.getSharedPreferences().getBoolean(TheConstants.SWITCHONOFF,true)) {
                    if (!endClick) {
                        closeWindow();
                    }
                    endClick = false;
                }
                break;
            case TelephonyManager.CALL_STATE_OFFHOOK:
                Log.i(TAG, "[Broadcast]通话中=" + phoneNumber);
                closeWindow();
                break;
        }
    }

    private void checkPhoneNum(Context context, String phoneNumber) {
        createDesk(MyApplication.getInstance(), phoneNumber);
    }

    private void createDesk(Context context, String phoneNum) {
        Log.e("coming","coming");
        AudioManager audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        audioManager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
        audioManager.setVibrateSetting(AudioManager.VIBRATE_TYPE_RINGER,AudioManager.VIBRATE_SETTING_OFF);
        audioManager.setVibrateSetting(AudioManager.VIBRATE_TYPE_NOTIFICATION,AudioManager.VIBRATE_SETTING_ON);
        manager = (WindowManager) MyApplication.getInstance().getSystemService(Context.WINDOW_SERVICE);
        mLayout = new WindowManager.LayoutParams();
        mLayout.width = MeasureUtil.getWidownSize(context).get(0);
        mLayout.height = MeasureUtil.getWidownSize(context).get(1);
        mLayout.type =  WindowManager.LayoutParams.TYPE_TOAST;
        mLayout.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;

        if (Build.VERSION.SDK_INT >= 21) {
            mLayout.width = MeasureUtil.getWidownSize(context).get(0);
            mLayout.height = MeasureUtil.getWidownSize(context).get(1);
            int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0) {
                mLayout.y = 0 - context.getResources().getDimensionPixelSize(resourceId);
            }
        }


        // 设置窗体焦点及触摸：
        // FLAG_NOT_FOCUSABLE(不能获得按键输入焦点)
        mLayout.flags =  WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS;

        deskLayout = new DeskLayout(context, handler, phoneNum);
        manager.addView(deskLayout, mLayout);

    }

    public void acceptCall(Context context) {
        if (Build.VERSION.SDK_INT >= 21) {
            Intent answerCalintent = new Intent(context, AcceptCallActivity.class);
            answerCalintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            answerCalintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(answerCalintent);
        } else {
            Intent intent = new Intent(context, AcceptCallActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }

}
