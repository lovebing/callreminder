package com.wangniu.sevideo.reciver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.util.Log;

import com.wangniu.sevideo.bean.VideoInfo;
import com.wangniu.sevideo.util.GetLocalVideoImg;

public class VideoProvider implements AbstructProvider {
    private Context context;

    public VideoProvider(Context context) {
        this.context = context;
    }

    @Override
    public List<VideoInfo> getList() {
        List<VideoInfo> list = null;
        ContentResolver cr = context.getContentResolver();
        if (context != null) {
            Cursor cursor = cr.query(
                    MediaStore.Video.Media.EXTERNAL_CONTENT_URI, null, null,
                    null, null);
            if (cursor != null) {
                list = new ArrayList<VideoInfo>();
                while (cursor.moveToNext()) {
                    int id = cursor.getInt(cursor
                            .getColumnIndexOrThrow(MediaStore.Video.Media._ID));
//                    String title = cursor
//                            .getString(cursor
//                                    .getColumnIndexOrThrow(MediaStore.Video.Media.TITLE));
//                    String album = cursor
//                            .getString(cursor
//                                    .getColumnIndexOrThrow(MediaStore.Video.Media.ALBUM));
//                    String artist = cursor
//                            .getString(cursor
//                                    .getColumnIndexOrThrow(MediaStore.Video.Media.ARTIST));
                    String displayName = cursor
                            .getString(cursor
                                    .getColumnIndexOrThrow(MediaStore.Video.Media.DISPLAY_NAME));
//                    String mimeType = cursor
//                            .getString(cursor
//                                    .getColumnIndexOrThrow(MediaStore.Video.Media.MIME_TYPE));
                    String path = cursor
                            .getString(cursor
                                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA));
//                    long duration = cursor
//                            .getInt(cursor
//                                    .getColumnIndexOrThrow(MediaStore.Video.Media.DURATION));
//                    long size = cursor
//                            .getLong(cursor
//                                    .getColumnIndexOrThrow(MediaStore.Video.Media.SIZE));
//                    int imgId = cursor
//                            .getInt(cursor
//                                    .getColumnIndexOrThrow(MediaStore.Images.Thumbnails.IMAGE_ID));
                    VideoInfo video = new VideoInfo();
                    video.setDisplayName(displayName);
                    video.setPath(path);
                    video.setId(id);
//                    Cursor cursor1 = cr.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
//                            new String[]{
//                                    MediaStore.Images.Media.DATA
//                            },
//                            MediaStore.Audio.Media._ID+"="+imgId,
//                            null,
//                            null
//                    );
//                    if (cursor1.moveToFirst()) {
//                        do {
//                            video.setImgUrl(cursor1.getColumnName(0));
//                        } while (cursor.moveToNext());
//                        cursor1.close();
//                    }
                    list.add(video);
                }
                cursor.close();
            }

        }
//        ArrayList<HashMap<String,String>> pics = GetLocalVideoImg.getAllPictures(context);
//        for (HashMap<String,String> map : pics){
//        }
        Collections.reverse(list);
        return list;
    }

}
