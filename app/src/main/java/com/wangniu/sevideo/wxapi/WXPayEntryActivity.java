package com.wangniu.sevideo.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.tencent.mm.sdk.constants.ConstantsAPI;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.wangniu.sevideo.MyApplication;
import com.wangniu.sevideo.util.L;
import com.wangniu.sevideo.util.TheConstants;

public class WXPayEntryActivity extends Activity implements IWXAPIEventHandler {
	
	private static final String TAG = "[LM-WXPay]";

	public static Handler orderingHandler = null;
	
    private IWXAPI api;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

		api = WXAPIFactory.createWXAPI(this, TheConstants.WECHAT_PAY_MCH_ID);

        api.handleIntent(getIntent(), this);
    }

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
        api.handleIntent(intent, this);
	}

	@Override
	public void onReq(BaseReq req) {
		L.i(TAG, "onReq, req:" + req.toString());
	}

	@Override
	public void onResp(BaseResp resp) {
		L.i(TAG, "onPayFinish, errCode = " + resp.errCode + ",errStr = " + resp.errStr);
		if(resp.errCode == 0) {
			MyApplication.getSharedPreferences().edit().putInt(TheConstants.SETTIME,2).commit();
			//Success
			if (resp.getType() == ConstantsAPI.COMMAND_PAY_BY_WX) {
				if(orderingHandler != null) {
					Message msg = orderingHandler.obtainMessage(TheConstants.MSG_PAYMENT_WXPAY_SUCESS);
					msg.sendToTarget();
				}
				finish();
			}
		} else {
			//failure
			if(orderingHandler != null) {
				Message msg = orderingHandler.obtainMessage(TheConstants.MSG_PAYMENT_WXPAY_FAILURE);
				msg.sendToTarget();
			}
			finish();
		}


	}
}