package com.wangniu.sevideo.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wangniu.sevideo.R;


/**
 * Created by Administrator on 2016/11/18 0018.
 */
public class VideoSettingDialog extends Dialog {

    private Context mContext;
    private int windowWidth;
    private LinearLayout llContent;
    private TextView btnSetDefault;
    private TextView btnSetFriend;
    private Handler mHandler ;
    public static final int SETVIDEODEFAULT = 13400;
    public static final int SETVIDEOFRIEND = 12387;

    public VideoSettingDialog(Context context,Handler handler) {
        super(context, R.style.dialog_style_base);
        this.mContext = context;
        mHandler = handler;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_video_setting);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        DisplayMetrics metric = getContext().getResources().getDisplayMetrics();
        lp.width = metric.widthPixels;
        lp.gravity = Gravity.CENTER;
        getWindow().setAttributes(lp);
        windowWidth = lp.width;
        initView();
    }

    private void initView() {
        int mHeight = 0;
        llContent = (LinearLayout) findViewById(R.id.ll_main);
        ViewGroup.LayoutParams params = llContent.getLayoutParams();
        params.width = windowWidth*4/5;
        params.height = params.width*9/13;
        llContent.setLayoutParams(params);

        btnSetDefault = (TextView) findViewById(R.id.btn_set_default);
        btnSetDefault.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHandler.sendEmptyMessage(SETVIDEODEFAULT);
            }
        });

        btnSetFriend = (TextView) findViewById(R.id.btn_set_friend);
        btnSetFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHandler.sendEmptyMessage(SETVIDEOFRIEND);
            }
        });

    }



}
