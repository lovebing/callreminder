package com.wangniu.sevideo.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wangniu.sevideo.R;

/**
 * Created by Administrator on 2016/11/21 0021.
 */
public class QuitDialog extends Dialog implements View.OnClickListener {

    private int windowWidth;
    private Context mContext;
    private LinearLayout llContent;
    private Handler mHandler;
    public static final int CLICKCONFIRM = 177664;
    public static final int CLICKCANCLE = 177851;
    private int type;
    private TextView tvConfirm;
    private TextView tvCancel;

    public QuitDialog(Context context, Handler handler) {
        super(context, R.style.dialog_style_base);
        this.mContext = context;
        this.mHandler = handler;
    }

    public QuitDialog(Context context, Handler handler, int type) {
        super(context, R.style.dialog_style_base);
        this.mContext = context;
        this.mHandler = handler;
        this.type = type;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_quit);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        DisplayMetrics metric = getContext().getResources().getDisplayMetrics();
        lp.width = metric.widthPixels;
        lp.gravity = Gravity.BOTTOM;
        getWindow().setAttributes(lp);
        windowWidth = lp.width;
        initView();
    }

    private void initView() {
        int mHeight = 0;
        llContent = (LinearLayout) findViewById(R.id.ll_main);
        ViewGroup.LayoutParams params = llContent.getLayoutParams();
        params.width = windowWidth;
        params.height = params.width*1/2;
        llContent.setLayoutParams(params);

        tvConfirm = (TextView) findViewById(R.id.tv_confirm);
        tvConfirm.setOnClickListener(this);
        tvCancel = (TextView) findViewById(R.id.tv_cancel);
        tvCancel.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_confirm:
                mHandler.sendEmptyMessage(CLICKCONFIRM);
                break;
            case R.id.tv_cancel:
                mHandler.sendEmptyMessage(CLICKCANCLE);
                break;
        }
    }
}
