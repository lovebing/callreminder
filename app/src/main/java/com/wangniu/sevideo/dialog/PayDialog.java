package com.wangniu.sevideo.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

import com.wangniu.sevideo.R;


/**
 * Created by Administrator on 2016/11/18 0018.
 */
public class PayDialog extends Dialog {

    private Context mContext;
    private int windowWidth;
    private Button btnKnow;
    private Handler mHandler;
    public static final int TOPAY = 18908;

    private LinearLayout llContent;
    public PayDialog(Context context,Handler handler) {
        super(context, R.style.dialog_style_base);
        this.mContext = context;
        this.mHandler = handler;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_pay);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        DisplayMetrics metric = getContext().getResources().getDisplayMetrics();
        lp.width = metric.widthPixels;
        lp.gravity = Gravity.CENTER;
        getWindow().setAttributes(lp);
        windowWidth = lp.width;
        initView();
    }

    private void initView() {
        int mHeight = 0;
        llContent = (LinearLayout) findViewById(R.id.ll_praise);
        ViewGroup.LayoutParams params = llContent.getLayoutParams();
        params.width = windowWidth*4/5;
        params.height = params.width*7/10;
        llContent.setLayoutParams(params);
        btnKnow = (Button) findViewById(R.id.btn_know);
        btnKnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHandler.sendEmptyMessage(TOPAY);
                dismiss();
            }
        });
    }



}
