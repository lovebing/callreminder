package com.wangniu.sevideo.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wangniu.sevideo.R;

import org.lovebing.android.oem.Vendor;
import org.lovebing.android.oem.VendorFactory;

/**
 * Created by Administrator on 2016/11/21 0021.
 */
public class GoSettingDialog extends Dialog {

    private int windowWidth;
    private Button btnSetting;
    private Button btnNotificationSetting;
    private Button btnAutoStartSetting;
    private Context mContext;
    private LinearLayout llContent;
    private TextView tvReminder;
    private Handler mHandler;
    public static final int TOSETTINGHOME = 144090;
    public static final int TOSETTINGWIDOWN = 141190;
    private int type;

    public GoSettingDialog(Context context,Handler handler) {
        super(context, R.style.dialog_style_base);
        this.mContext = context;
        this.mHandler = handler;
    }

    public GoSettingDialog(Context context,Handler handler,int type) {
        super(context, R.style.dialog_style_base);
        this.mContext = context;
        this.mHandler = handler;
        this.type = type;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_setting);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        DisplayMetrics metric = getContext().getResources().getDisplayMetrics();
        lp.width = metric.widthPixels;
        lp.gravity = Gravity.CENTER;
        getWindow().setAttributes(lp);
        windowWidth = lp.width;
        initView();
    }

    private void initView() {
        int mHeight = 0;
        llContent = (LinearLayout) findViewById(R.id.ll_praise);
        ViewGroup.LayoutParams params = llContent.getLayoutParams();
        params.width = windowWidth*4/5;
        //params.height = params.width*11/13;
        llContent.setLayoutParams(params);
        btnSetting = (Button) findViewById(R.id.btn_tosetting);
        btnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type == 1) {
                    mHandler.sendEmptyMessage(TOSETTINGWIDOWN);
                } else {
                    mHandler.sendEmptyMessage(TOSETTINGHOME);
                }
                dismiss();
            }
        });
        tvReminder = (TextView) findViewById(R.id.tv_reminder);
        if (type == 1){
            tvReminder.setText(mContext.getResources().getString(R.string.settingRemind2));
        } else if (type == 0){
            tvReminder.setText(mContext.getResources().getString(R.string.settingRemind));
        } else if (type == 2) {
            tvReminder.setVisibility(View.GONE);
            btnSetting.setVisibility(View.GONE);
        }

        final Vendor vendor = VendorFactory.getVendor(mContext);

        btnNotificationSetting = (Button) findViewById(R.id.btn_notification_setting);
        btnNotificationSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vendor.openNotificationSetting();
            }
        });
        btnAutoStartSetting = (Button) findViewById(R.id.btn_auto_start_setting);
        btnAutoStartSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vendor.openAutoStartSetting();
            }
        });
        if (vendor.getName().equals(Vendor.Name.GENERAL)) {
            btnNotificationSetting.setVisibility(View.GONE);
        }
        if (vendor.notificationListenersEnabled()) {
            btnNotificationSetting.setVisibility(View.GONE);
        }


    }
}
