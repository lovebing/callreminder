package com.wangniu.sevideo.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.wangniu.sevideo.bean.Contact;
import com.wangniu.sevideo.bean.DownloadInfo;
import com.wangniu.sevideo.bean.LocalContact;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2016/12/8 0008.
 */
public class BDDatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "callreminder.db";

    private static final int VERSION = 1;
    // table to store the power sample every hour
    private static final String TABLE_DOWNLOAD_FILE = "download_info";
    private static final String TABLE_CONTACTS_FILE = "contacts_info";
    private static final String TABLE_CONTACTS_FILE_SELECT = "contacts_info_select";
    private static final String FILE_ID = "id";
    private static final String FILE_URL = "url";
    private static final String FILE_NAME = "path";
    private static final String FILE_IMG = "img";
    private static final String CONTACTS_NAME = "name";
    private static final String CONTACTS_NUM = "num";
    private static final String SELCTED_ID = "se_id";

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");


    private String CREATE_TABLE_DOWLOAD_FILE = "CREATE TABLE " + TABLE_DOWNLOAD_FILE +
            "([" + FILE_ID + "] INTEGER PRIMARY KEY, " +
            "[" + FILE_NAME + "] VARCHAR(100) NOT NULL, " +
             "[" + FILE_IMG + "] VARCHAR(1024) NOT NULL, "+
            "[" + FILE_URL + "] VARCHAR(1024) NOT NULL )";

    private String CREATE_TABLE_CONTACTS_FILE = "CREATE TABLE " + TABLE_CONTACTS_FILE +
            "([" + FILE_ID + "] INTEGER PRIMARY KEY, " +
            "[" + CONTACTS_NAME + "] VARCHAR(10) NOT NULL, " +
            "[" + CONTACTS_NUM + "] VARCHAR(20) NOT NULL )";

    private String CREATE_TABLE_CONTACTS_FILE_SELECT = "CREATE TABLE " + TABLE_CONTACTS_FILE_SELECT +
            "([" + FILE_ID + "] INTEGER PRIMARY KEY, " +
            "[" + CONTACTS_NAME + "] VARCHAR(10) NOT NULL, " +
            "[" + SELCTED_ID + "] VARCHAR(20), " +
            "[" + CONTACTS_NUM + "] VARCHAR(20) NOT NULL )";


    public BDDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_DOWLOAD_FILE);
        sqLiteDatabase.execSQL(CREATE_TABLE_CONTACTS_FILE);
        sqLiteDatabase.execSQL(CREATE_TABLE_CONTACTS_FILE_SELECT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    }

    /****
     * Following functions related with BatteryHistory
     */

    public List<DownloadInfo> queryAll() {
        List<DownloadInfo> result = new ArrayList<DownloadInfo>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_DOWNLOAD_FILE ;
        Cursor cursor = db.rawQuery(selectQuery, null);
        while(cursor.moveToNext()) {
            DownloadInfo info = new DownloadInfo();
            Integer id = cursor.getInt(cursor.getColumnIndex(FILE_ID));
            String url = cursor.getString(cursor.getColumnIndex(FILE_URL));
            String name = cursor.getString(cursor.getColumnIndex(FILE_NAME));
            String img = cursor.getString(cursor.getColumnIndex(FILE_IMG));
            if(id<1008688){
                info.setId(id);
                info.setUrl(url);
                info.setName(name);
                info.setImg(img);
                result.add(info);
            }

        }
        cursor.close();
        return result;
    }

    public DownloadInfo queryById(int mId) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_DOWNLOAD_FILE + " WHERE id=" + mId;
        Cursor cursor = db.rawQuery(selectQuery, null);
        while(cursor.moveToNext()) {
            DownloadInfo info = new DownloadInfo();
            Integer id = cursor.getInt(cursor.getColumnIndex(FILE_ID));
            String url = cursor.getString(cursor.getColumnIndex(FILE_URL));
            String name = cursor.getString(cursor.getColumnIndex(FILE_NAME));
            String img = cursor.getString(cursor.getColumnIndex(FILE_IMG));
            info.setId(id);
            info.setUrl(url);
            info.setName(name);
            info.setImg(img);
            cursor.close();
            return info;
        }
        cursor.close();
        return null;
    }


    public void add(DownloadInfo downloadInfo){
        try{
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            Log.e("downloadinfo",downloadInfo.getImg());
            values.put(FILE_ID,downloadInfo.getId());
            values.put(FILE_URL,downloadInfo.getUrl());
            values.put(FILE_NAME,downloadInfo.getName());
            values.put(FILE_IMG,downloadInfo.getImg());
            db.replace(TABLE_DOWNLOAD_FILE,null,values);
        }catch(Exception e){
            Log.e("videoshare", "record duplicate:" + downloadInfo);
        }
    }

    public void addContacts(List<LocalContact> contacts){
        try{
            SQLiteDatabase db = this.getWritableDatabase();
            for (int i = 0;i<contacts.size();i++){//Map<String,String>contact : contacts
                ContentValues values = new ContentValues();
                values.put(FILE_ID,contacts.get(i).getId());
                values.put(CONTACTS_NAME,contacts.get(i).getName());
                values.put(CONTACTS_NUM,contacts.get(i).getNum());
                db.replace(TABLE_CONTACTS_FILE,null,values);
            }
        }catch(Exception e){

        }
    }

    public void addContactsSelect(List<Contact> contacts){
        try{
            SQLiteDatabase db = this.getWritableDatabase();
            for (int i = 0;i<contacts.size();i++){//Map<String,String>contact : contacts
                ContentValues values = new ContentValues();
                values.put(FILE_ID,contacts.get(i).getId());
                values.put(CONTACTS_NAME,contacts.get(i).getName());
                values.put(CONTACTS_NUM,contacts.get(i).getNum());
                values.put(SELCTED_ID,contacts.get(i).getsId());
                db.replace(TABLE_CONTACTS_FILE_SELECT,null,values);
            }
        }catch(Exception e){

        }
    }

    public String queryContact(String num) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CONTACTS_FILE + " WHERE num=" + num ;
        Cursor cursor = db.rawQuery(selectQuery, null);
        while (cursor.moveToNext()){
            String name = cursor.getString(cursor.getColumnIndex(CONTACTS_NAME));
            cursor.close();
            return name;
        }
        cursor.close();
        return "";
    }

    public Contact queryContactSelect(String num) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CONTACTS_FILE_SELECT + " WHERE num=" + num ;
        Cursor cursor = db.rawQuery(selectQuery, null);
        while (cursor.moveToNext()){
            Contact contact = new Contact();
            String name = cursor.getString(cursor.getColumnIndex(CONTACTS_NAME));
            String phoneNum = cursor.getString(cursor.getColumnIndex(CONTACTS_NUM));
            String sId = cursor.getString(cursor.getColumnIndex(SELCTED_ID));
            int id = cursor.getInt(cursor.getColumnIndex(FILE_ID));
            contact.setName(name);
            contact.setsId(sId);
            contact.setNum(phoneNum);
            contact.setId(id);
            cursor.close();
            return contact;
        }
        cursor.close();
        return null;
    }

    public List<Contact> queryAllContacts() {
        SQLiteDatabase db = this.getReadableDatabase();
        List<Contact> contacts = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_CONTACTS_FILE;
        Cursor cursor = db.rawQuery(selectQuery, null);
        while(cursor.moveToNext()) {
            Contact contact = new Contact();
            String name = cursor.getString(cursor.getColumnIndex(CONTACTS_NAME));
            contact.setName(name);
            String num = cursor.getString(cursor.getColumnIndex(CONTACTS_NUM));
            contact.setNum(num);
            int id = cursor.getInt(cursor.getColumnIndex(FILE_ID));
            contact.setId(id);
            contacts.add(contact);
        }
        cursor.close();
        return contacts;
    }

    public List<Contact> queryAllContactsSelect() {
        SQLiteDatabase db = this.getReadableDatabase();
        List<Contact> contacts = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_CONTACTS_FILE_SELECT;
        Cursor cursor = db.rawQuery(selectQuery, null);
        while(cursor.moveToNext()) {
            Contact contact = new Contact();
            String name = cursor.getString(cursor.getColumnIndex(CONTACTS_NAME));
            contact.setName(name);
            String num = cursor.getString(cursor.getColumnIndex(CONTACTS_NUM));
            contact.setNum(num);
            int id = cursor.getInt(cursor.getColumnIndex(FILE_ID));
            contact.setId(id);
            String sId = cursor.getString(cursor.getColumnIndex(SELCTED_ID));
            contact.setsId(sId);
            contacts.add(contact);
        }
        cursor.close();
        return contacts;
    }

    public void delteAllContacts() {
        SQLiteDatabase db = this.getReadableDatabase();
        db.delete(TABLE_CONTACTS_FILE, "", new String[]{});
    }

    public void updataContactsSId(String name,String id){
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(SELCTED_ID,id);
        String[] args = { name };
        db.update(TABLE_CONTACTS_FILE, cv, "name=?", args);
    }

    public void updataContactsSId(int conId,String id){
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(SELCTED_ID,id);
        String cId = String.valueOf(conId);
        String[] args = { cId };
        db.update(TABLE_CONTACTS_FILE,cv,"id=?",args);
    }
}
