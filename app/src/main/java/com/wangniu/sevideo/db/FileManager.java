package com.wangniu.sevideo.db;

import com.wangniu.sevideo.MyApplication;
import com.wangniu.sevideo.bean.Contact;
import com.wangniu.sevideo.bean.DownloadInfo;
import com.wangniu.sevideo.bean.LocalContact;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

/**
 * Created by zhangyu on 16-11-4.
 */
public class FileManager {
    static BDDatabaseHelper helper = new BDDatabaseHelper(MyApplication.getInstance());
    static private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    public static DownloadInfo queryById(int mId){
        return helper.queryById(mId);
    }

    public static void addInfo(DownloadInfo info){
        helper.add(info);
    }

    public static List<DownloadInfo> getAllInfo(){
       return helper.queryAll();
    }
    public static void addContants(List<LocalContact> contacts){
        helper.addContacts(contacts);
    }
    public static String queryContact(String num){
        return helper.queryContact(num);
    }
    public static List<Contact> queryAllContact(){
        return helper.queryAllContacts();
    }
    public static List<Contact> queryAllContactSelect(){
        return helper.queryAllContactsSelect();
    }
    public static void deleteAllContact(){
        helper.delteAllContacts();
    }

    public static void updataContactsSId(String name,String id){helper.updataContactsSId(name, id);};
    public static void updataContactsSId(int conId,String id){helper.updataContactsSId(conId,id);};
    public static void insertContactsSId(List<Contact> contacts){helper.addContactsSelect(contacts);};
    public static Contact queryContactSelect(String num){return helper.queryContactSelect(num);};
}
