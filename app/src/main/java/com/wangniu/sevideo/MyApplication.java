package com.wangniu.sevideo;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.text.TextUtils;

import com.alibaba.sdk.android.feedback.impl.FeedbackAPI;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.tencent.bugly.crashreport.CrashReport;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.wangniu.sevideo.service.KeepLiveService;
import com.wangniu.sevideo.util.CallUtils;
import com.wangniu.sevideo.util.TheConstants;

import java.util.List;

/**
 * Created by jason on 3/4/16.
 */
public class MyApplication extends Application {

    private static final String TAG = "[LL-APP]";
    private static MyApplication APP_INSTANCE;
    private com.android.volley.toolbox.ImageLoader volleyImageLoader;

    private RequestQueue volleyRequestQueue;

    public static synchronized MyApplication getInstance(){
        return APP_INSTANCE;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        APP_INSTANCE = this;
        FeedbackAPI.init(MyApplication.getInstance(), "23561932");
        CrashReport.initCrashReport(getApplicationContext(), "21410a78a7", false);

    }

    public static SharedPreferences getSharedPreferences() {
        return getInstance().getSharedPreferences(TheConstants.PREFERENCE_FILE, Context.MODE_PRIVATE);
    }

    public static SharedPreferences.Editor getSharedPreferencesEditor() {
        return getInstance().getSharedPreferences(TheConstants.PREFERENCE_FILE, Context.MODE_PRIVATE).edit();
    }

    public RequestQueue getRequestQueue(){
        if (volleyRequestQueue == null){
            volleyRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return volleyRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req){
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public com.android.volley.toolbox.ImageLoader getVolleyImageLoader() {
        if (volleyImageLoader == null) {
            volleyImageLoader = new com.android.volley.toolbox.ImageLoader(getRequestQueue(), new VollyBitmapCache());
        }
        return volleyImageLoader;
    }

    private class VollyBitmapCache implements com.android.volley.toolbox.ImageLoader.ImageCache {

        private LruCache<String, Bitmap> mCache;

        public VollyBitmapCache() {
            int maxSize = 20 * 1024 * 1024;
            mCache = new LruCache<String, Bitmap>(maxSize) {
                @Override
                protected int sizeOf(String key, Bitmap bitmap) {
                    return bitmap.getRowBytes() * bitmap.getHeight();
                }
            };
        }

        @Override
        public Bitmap getBitmap(String url) {
            return mCache.get(url);
        }

        @Override
        public void putBitmap(String url, Bitmap bitmap) {
            mCache.put(url, bitmap);
        }

    }

    @Override
    public void onTerminate() {
        //程序终止的时候执行
        super.onTerminate();
    }

    @Override
    public void onLowMemory() {
        //低内存的时候执行
        super.onLowMemory();
    }

    @Override
    public void onTrimMemory(int level) {
        //程序在内存清理的时候执行
        super.onTrimMemory(level);
    }

    public void cancelPendingRequests(Object tag) {
        if (volleyRequestQueue != null) {
            volleyRequestQueue.cancelAll(tag);
        }
    }

}
