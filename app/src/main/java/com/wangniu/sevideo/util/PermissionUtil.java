package com.wangniu.sevideo.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.provider.Settings;

import org.lovebing.android.oem.VendorFactory;

/**
 * Created by lovebing on 2017/12/3.
 */
public class PermissionUtil {

    public static void checkPermission(final Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (!Settings.Secure.getString(context.getContentResolver(), "enabled_notification_listeners").contains(context.getApplicationContext().getPackageName())) {
                new AlertDialog.Builder(context)
                        .setMessage("为了可以接电话，请开启通知使用权")
                        .setPositiveButton("马上设置", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                VendorFactory.getVendor(context).openNotificationSetting();
                            }
                        })
                        .create().show();
            }
        }
    }
}
