package com.wangniu.sevideo.util;

import android.content.Context;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/11/17 0017.
 */
public class MeasureUtil {

    //获取屏幕宽高
    public static List<Integer> getWidownSize(Context context){
        List<Integer> sizes = new ArrayList<>();
        WindowManager wm = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        int width = wm.getDefaultDisplay().getWidth();
        int height = wm.getDefaultDisplay().getHeight();
        sizes.add(width);
        sizes.add(height);
        return sizes;
    }
}
