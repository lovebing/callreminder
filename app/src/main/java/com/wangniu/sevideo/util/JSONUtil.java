package com.wangniu.sevideo.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class JSONUtil {

	public static double getDouble(JSONObject json, String key) {
		return getDouble(json, key, 0);
	}

	public static double getDouble(JSONObject json, String key, double def) {
		if(json == null || json.isNull(key)){
			return def;
		}
		try {
			return json.getDouble(key);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return def;
	}

	public static int getInt(JSONObject json, String key){
		return getInt(json, key, 0);
	}
	
	public static int getInt(JSONObject json, String key, int def){
		if(json == null || json.isNull(key)){
			return def;
		}
		try {
			return json.getInt(key);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return def;
	}

	public static long getLong(JSONObject json, String key){
		return getLong(json,key,0);
	}

	public static long getLong(JSONObject json, String key, int def){
		if(json == null || json.isNull(key)){
			return def;
		}
		try {
			return json.getLong(key);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return def;
	}
	
	public static String getString(JSONObject json, String key){
		return getString(json,key,"");
	}
	
	public static String getString(JSONObject json, String key, String def){
		if(json == null || json.isNull(key)){
			return def;
		}
		try {
			return json.getString(key);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return def;
	}


	public static JSONObject getJSON(JSONObject json, String key) {
		if(json != null){
			try {
				return json.getJSONObject(key);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static JSONObject getJSONByIndex(JSONArray json, int index) {
		if(json != null){
			try {
				return json.getJSONObject(index);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public static Map<String, String> getMaps(JSONObject root, String name) {
		try{
			JSONObject json = getJSON(root, name);
			if(json == null){
				return null;
			}
			Iterator<String> it = json.keys();
			Map<String,String> map = new HashMap<String,String>();
			if(it!=null){
				while(it.hasNext()){
					String key = it.next();
					map.put(key, json.getString(key));
				}
				return map;
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public static JSONObject getJSON(String value) {
		if(!StringUtil.isEmpty(value)){
			try {
				return new JSONObject(value);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static JSONArray getJSONArray(JSONObject json, String key) {
		if(json != null)
			try {
				return json.getJSONArray(key);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		return null;
	}

	public static JSONObject[] getJSONArray2(JSONObject json, String key) {
		if(json!=null)
			try {
				JSONArray array = json.getJSONArray(key);
				if(array!=null){
					JSONObject[] objs = new JSONObject[array.length()];
					for(int i=0;i<array.length();i++){
						objs[i] = array.getJSONObject(i);
					}
					return objs;
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return null;
	}

	public static void put(JSONObject as, String name, String package_name) {
		try {
			as.put(name, package_name);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public static void put(JSONObject as, String name, int value) {
		try {
			as.put(name, value);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public static void put(JSONObject json, String name, JSONArray array) {
		try {
			json.put(name, array);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public static boolean getBoolean(JSONObject json, String key, boolean def) {
		if(json == null || json.isNull(key)){
			return def;
		}
		try {
			return json.getBoolean(key);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return def;
	}
}
