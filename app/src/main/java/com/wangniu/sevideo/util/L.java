package com.wangniu.sevideo.util;

import android.os.Environment;
import android.util.Log;

import com.wangniu.sevideo.BuildConfig;

import java.io.File;
import java.io.RandomAccessFile;
import java.sql.Date;
import java.text.SimpleDateFormat;

/**
 * android.util.Log wrapper
 *
 * Updated by Jason
 */
public class L {

	private static final String TAG = "[LM-L]";

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");

	public static void v(String tag, String message) {
		if(BuildConfig.DEBUG && message != null) {
			Log.v(tag, message);
		}
	}

	public static void v(String tag, String message, Throwable t) {
		if(BuildConfig.DEBUG && message != null) {
			Log.v(tag, message, t);
		}
	}

	public static void d(String tag, String message) {
		if (BuildConfig.DEBUG && message != null) {
			Log.d(tag, message);
		}
	}

	public static void d(String tag, String message, Throwable t) {
		if (BuildConfig.DEBUG && message != null) {
			Log.d(tag, message, t);
		}
	}

	public static void i(String tag, String message) {
		if (BuildConfig.DEBUG && message != null) {
			Log.i(tag, message);
		}
	}

	public static void i(String tag, String message, Throwable t) {
		if (BuildConfig.DEBUG && message != null) {
			Log.i(tag, message, t);
		}
	}

	public static void w(String tag, String message) {
		if (BuildConfig.DEBUG && message != null) {
			Log.w(tag, message);
		}
	}

	public static void w(String tag, String message, Throwable t) {
		if (BuildConfig.DEBUG && message != null) {
			Log.w(tag, message, t);
		}
	}

	public static void e(String tag, String message) {
		if (BuildConfig.DEBUG && message != null) {
			Log.e(tag, message);
		}
	}

	public static void e(String tag, String message, Throwable t) {
		if (BuildConfig.DEBUG && message != null) {
			Log.e(tag, message, t);
		}
	}

	public static void toSD(String tag, String message) {
		if(BuildConfig.DEBUG && message != null) {
			StringBuilder sbPath = new StringBuilder(Environment.getExternalStorageDirectory().getAbsolutePath() + "/luckymoney/");
			try {
				File logPath = new File(sbPath.toString());
				if (!logPath.exists()) {
					w(TAG, "create log folder " + sbPath.toString());
					logPath.mkdir();
				}
				File logFile = new File(sbPath.append("log.txt").toString());
				if (!logFile.exists()) {
					w(TAG, "create log file " + sbPath.append("log.txt").toString());
					logFile.createNewFile();
				}
				RandomAccessFile raf = new RandomAccessFile(logFile, "rw");
				raf.seek(logFile.length());
				Date time = new Date(System.currentTimeMillis());
				String strOutput = sdf.format(time) + "## " + tag + "#" + message + "\n";
				raf.write(strOutput.getBytes());
				raf.close();
			} catch(Exception e) {
				e.printStackTrace();
			}
		} else {
			i(tag, message);
		}

	}
}
