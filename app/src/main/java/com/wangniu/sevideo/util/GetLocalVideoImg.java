package com.wangniu.sevideo.util;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2017/1/3 0003.
 */
public class GetLocalVideoImg {
    public static MediaMetadataRetriever media =new MediaMetadataRetriever();
    public static Bitmap getVideoThumbnail1(String videoPath) {
        media =new MediaMetadataRetriever();
        media.setDataSource(videoPath);
        Bitmap bitmap = media.getFrameAtTime();
        return bitmap;
    }
    public static Bitmap getVideoThumbnail(String videoPath) {
        Bitmap bitmap = null;
        // 获取视频的缩略图
        bitmap = ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Images.Thumbnails.MINI_KIND);
//        System.out.println("w"+bitmap.getWidth());
//        System.out.println("h"+bitmap.getHeight());
//        bitmap = ThumbnailUtils.extractThumbnail(bitmap, width, height,
//                ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
        return bitmap;
    }

//    public static ArrayList<HashMap<String,String>> getAllPictures(Context context) {
//        ArrayList<HashMap<String,String>> picturemaps = new ArrayList<>();
//        HashMap<String,String> picturemap;
//        ContentResolver cr = context.getContentResolver();
//        //先得到缩略图的URL和对应的图片id
//        Cursor cursor = cr.query(
//                MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI,
//                new String[]{
//                        MediaStore.Images.Thumbnails.IMAGE_ID,
//                        MediaStore.Images.Thumbnails.DATA
//                },
//                null,
//                null,
//                null);
//        if (cursor.moveToFirst()) {
//            do {
//                picturemap = new HashMap<>();
//                picturemap.put("image_id_path",cursor.getInt(0)+"");
//                picturemap.put("thumbnail_path",cursor.getString(1));
//                picturemaps.add(picturemap);
//            } while (cursor.moveToNext());
//            cursor.close();
//        }
//        //再得到正常图片的path
//        for (int i = 0;i<picturemaps.size();i++) {
//            picturemap = picturemaps.get(i);
//            String media_id = picturemap.get("image_id_path");
//            cursor = cr.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
//                    new String[]{
//                            MediaStore.Images.Media.DATA
//                    },
//                    MediaStore.Audio.Media._ID+"="+media_id,
//                    null,
//                    null
//            );
//            if (cursor.moveToFirst()) {
//                do {
//                    picturemap.put("image_id",cursor.getString(0));
//                    picturemaps.set(i, picturemap);
//                    Log.e("==map==", cursor.getString(0));
//                } while (cursor.moveToNext());
//                cursor.close();
//            }
//        }
//        return picturemaps;
//    }

}
