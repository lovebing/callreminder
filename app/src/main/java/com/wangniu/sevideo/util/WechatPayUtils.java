package com.wangniu.sevideo.util;

import com.tencent.mm.sdk.modelpay.PayReq;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.wangniu.sevideo.MyApplication;
import com.wangniu.sevideo.bean.OrderPaymentStatusBean;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Created by jason on 4/14/16.
 */
public class WechatPayUtils {

    private static final IWXAPI msgApi = WXAPIFactory.createWXAPI(MyApplication.getInstance().getApplicationContext(), null);
    private static PayReq req = new PayReq();

    public static void pay(OrderPaymentStatusBean order) {
        genPayReq(order);
    }

    private static void genPayReq(OrderPaymentStatusBean payment) {

        req.appId = payment.getmAppId();
        req.partnerId = payment.getmMchId();
        req.prepayId = payment.getmPrepareOrderId();
        req.packageValue = "Sign=WXPay";
        req.nonceStr = genNonceStr();
        req.timeStamp = String.valueOf(genTimeStamp());

        List<NameValuePair> signParams = new LinkedList<NameValuePair>();
        signParams.add(new BasicNameValuePair("appid", req.appId));
        signParams.add(new BasicNameValuePair("noncestr", req.nonceStr));
        signParams.add(new BasicNameValuePair("package", req.packageValue));
        signParams.add(new BasicNameValuePair("partnerid", req.partnerId));
        signParams.add(new BasicNameValuePair("prepayid", req.prepayId));
        signParams.add(new BasicNameValuePair("timestamp", req.timeStamp));

        req.sign = genAppSign(signParams);
        msgApi.registerApp(payment.getmAppId());
        msgApi.sendReq(req);
    }

    private static String genNonceStr() {
        Random random = new Random();
        return MD5.getMessageDigest(String.valueOf(random.nextInt(10000)).getBytes());
    }

    private static long genTimeStamp() {
        return System.currentTimeMillis() / 1000;
    }

    private static String genAppSign(List<NameValuePair> params) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < params.size(); i++) {
            sb.append(params.get(i).getName());
            sb.append('=');
            sb.append(params.get(i).getValue());
            sb.append('&');
        }
        sb.append("key=");
        sb.append(TheConstants.WECHAT_PAY_API_KEY);
        String appSign = MD5.getMessageDigest(sb.toString().getBytes()).toUpperCase();
        return appSign;
    }
}
