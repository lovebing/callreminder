package com.wangniu.sevideo.util;

/**
 * Created by jason on 1/12/16.
 */
public interface TheConstants {

    // Url
    String URL_GET_VIDEO_INFOS = "http://news.intbull.com/lingsheng.jsp";//http://file.intbull.com/onemall/news/nn_icon.png
    String URL_HOST_PAY_PREFIX = "http://pay.intbull.com/";
    //http://file.intbull.com/onemall/news/lingsheng.html
    String URL_HOST_RELEASE = "http://onemall.intbull.com/";
    String URL_YYSC_PREFIX = URL_HOST_RELEASE + "yiyuan.jsp";
    String URL_PAY_PREFIX = URL_HOST_PAY_PREFIX + "pay.jsp";

    //final
    String PREFERENCE_FILE = "callReminder_settings";
    String RINGSAVEPATH = "SavePath";
    String SELECTID = "SelectID";
    String RINGSAVENAME = "SaveName";
    String SWITCHONOFF = "SwitchOnOFF";
    String SETTIME = "SetTimes";

    // wechat info
    String WECHAT_APP_ID = "wxd0001bd79f2a301f";
    String WECHAT_APP_SECRET = "a2731c1b06a68224c9afb5172d39816c";
    String WECHAT_PAY_MCH_ID = "1317441401";
    String WECHAT_PAY_API_KEY = "abcdefghijklmnopqrstuvwxyz012345";
    String KEY_ACCOUNT_WECHAT_OPEN_ID = "user_wechat_open_id";
    public static final String LUCKY_MONEY_LOGIN_WX_AT = "lm_login_wx_at";

    //wechat_pay
    int MSG_PAYMENT_WXPAY_SUCESS = 0x4402;
    int MSG_PAYMENT_WXPAY_FAILURE = 0x4403;
    int MSG_PAYMENT_WXPAY_FAUILRE = 0x6004;

    int MSG_PAYMENT_SUCCESS = 0x5001;
    int MSG_PAYMENT_FAULIRE = 0x5002;
    int MSG_PAYMENT_TIMEOUT = 0x5003;
    int MSG_PAYMENT_WXPAY_REQUIRED = 0x6001;

    //umeng
    String META_DATA_UMENG_CHANNEL = "UMENG_CHANNEL";

}
