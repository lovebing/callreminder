package com.wangniu.sevideo.util;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;

import com.google.zxing.common.BitMatrix;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Administrator on 2016/8/3 0003.
 */
public class ShareToTimelineUtil {
    private static String tmpImg1 = Environment.getExternalStorageDirectory().getAbsolutePath() + "/image_lock1.bmp";
    private static String tmpImg2 = Environment.getExternalStorageDirectory().getAbsolutePath() + "/image_lock2.bmp";
    private static String tmpImg3 = Environment.getExternalStorageDirectory().getAbsolutePath() + "/image_lock3.bmp";
//    private static String tmpQrPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/qr_lock.png";

    public static void shareToTimeline(String title,String linkUrl,Bitmap bitmap1,Bitmap bitmap2,Bitmap bitmap3,Context context){
        StringBuilder sb = new StringBuilder();
        File tmpImgFile1 = new File(tmpImg1);
        if(!tmpImgFile1.exists()) {
            try {
                tmpImgFile1.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        File tmpImgFile2 = new File(tmpImg2);
        if(!tmpImgFile1.exists()) {
            try {
                tmpImgFile2.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        File tmpImgFile3 = new File(tmpImg3);
        if(!tmpImgFile1.exists()) {
            try {
                tmpImgFile3.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

//        File tmpQrFile = new File(tmpQrPath);
//        if(!tmpQrFile.exists()) {
//            try {
//                tmpQrFile.createNewFile();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
        sb.append(title);//.append("\n").append("\uD83D\uDC47猛戳链接体验⬇️\n")
        sb.append(linkUrl);
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(tmpImgFile1);
            if (fileOutputStream != null && bitmap1 != null) {
                bitmap1.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
            }
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            FileOutputStream fileOutputStream = new FileOutputStream(tmpImgFile2);
            if (fileOutputStream != null && bitmap2 != null) {
                bitmap2.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
            }
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            FileOutputStream fileOutputStream = new FileOutputStream(tmpImgFile3);
            if (fileOutputStream != null && bitmap3 != null) {
                bitmap3.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
            }
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Generate qrcode img
//        try {
//            FileOutputStream fileOutputStream = new FileOutputStream(tmpQrFile);
//            QRCodeWriter writer = new QRCodeWriter();
//            BitMatrix matrix = writer.encode(linkUrl, BarcodeFormat.QR_CODE, 500, 500);
//            Bitmap bmpQr = bitMatrix2Bitmap(matrix);
//            Canvas canvas = new Canvas(bmpQr);
//            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//            paint.setColor(Color.rgb(61, 61, 61));
//            paint.setTextSize(35);
//            paint.setShadowLayer(1f, 0f, 1f, Color.WHITE);
//            // draw text to the Canvas center
//            Rect bounds = new Rect();
//            String strToPaint = "长按识别二维码可查看详情";
//            paint.getTextBounds(strToPaint, 0, strToPaint.length(), bounds);
//            int x = (bmpQr.getWidth() - bounds.width())/2;
//            int y = bounds.height() + 10;
//            canvas.drawText(strToPaint, x, y, paint);
//            if (fileOutputStream != null && bmpQr != null) {
//                bmpQr.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
//            }
//            fileOutputStream.flush();
//            fileOutputStream.close();
//        } catch (WriterException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        Intent intent = new Intent();
        ComponentName comp = new ComponentName("com.tencent.mm",
                "com.tencent.mm.ui.tools.ShareToTimeLineUI");
        intent.setComponent(comp);
        intent.setAction(Intent.ACTION_SEND_MULTIPLE);
        intent.setType("image/*");

        ArrayList<Uri> arrayImages = new ArrayList<>();
        arrayImages.add(Uri.fromFile(tmpImgFile1));
        arrayImages.add(Uri.fromFile(tmpImgFile2));
        arrayImages.add(Uri.fromFile(tmpImgFile3));
//        arrayImages.add(Uri.fromFile(tmpQrFile));
        intent.putExtra("Kdescription", sb.toString());
        intent.putExtra(Intent.EXTRA_STREAM, arrayImages);
        context.startActivity(intent);
    }

    private static Bitmap bitMatrix2Bitmap(BitMatrix matrix) {
        int w = matrix.getWidth();
        int h = matrix.getHeight();
        int[] rawData = new int[w * h];
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                int color = Color.WHITE;
                if (matrix.get(i, j)) {
                    color = Color.BLACK;
                }
                rawData[i + (j * w)] = color;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.RGB_565);
        bitmap.setPixels(rawData, 0, w, 0, 0, w, h);
        return bitmap;
    }
}
