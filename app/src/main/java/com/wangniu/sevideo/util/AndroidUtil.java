package com.wangniu.sevideo.util;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class AndroidUtil {

	public static String getImei(Context ctx){
		TelephonyManager tm = (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);
		return tm.getDeviceId();
	}
	
	public static String getImsi(Context ctx){
		TelephonyManager tm = (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);
		return tm.getSubscriberId();
	}
	
	//网络是否连接着
	public static boolean isConnected(Context ctx){
		ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfos = cm.getAllNetworkInfo();
		if (netInfos != null) {
			for (NetworkInfo ni : netInfos) {
				if (ni.isConnected()) {
					return true;
				}
			}
		}
		return false;
	}
	
	public static String getRunningSMS(Context ctx) throws NameNotFoundException {
		StringBuffer sb = new StringBuffer();
		Map<String,Integer> map = new HashMap<String,Integer>();
		ActivityManager am = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
		 //获取正在运行的应用  
		List<RunningAppProcessInfo> run = am.getRunningAppProcesses();  
		for(RunningAppProcessInfo ra : run){ 
			String[] packages = ra.pkgList;
			if(packages==null)
				continue;
			for(String package_name : packages){
				if(hasSMSPermisson(ctx,package_name)){
					if(!map.containsKey(package_name)){
						map.put(package_name,ra.pid);
					}else{
						if(map.get(package_name)>ra.pid){
							map.put(package_name,ra.pid);
						}
					}
				}
			}
		}
		
		Iterator<Map.Entry<String, Integer>> iterator = map.entrySet().iterator();
		while(iterator.hasNext()){
			Map.Entry<String, Integer> entry = iterator.next();
			sb.append(entry.getKey()+"|"+entry.getValue()+"_");			
		}
		
		String result = sb.toString();
		if(result.trim().length()!=0){
			return result.substring(0,result.length()-1);
		}else{
			return "";
		}
	}
		
	
	//判断一个包是否有短信权限
	public static boolean hasSMSPermisson(Context ctx, String package_name) throws NameNotFoundException{
		PackageManager manager = ctx.getPackageManager();
		PackageInfo pi = manager.getPackageInfo(package_name,PackageManager.GET_PERMISSIONS);
		if(pi==null || pi.requestedPermissions==null){
			return false;
		}
		for(String premission : pi.requestedPermissions){
			if(premission.equals("android.permission.RECEIVE_SMS")){
				return true;
			}
		}
		return false;
	}
	
	private final static int kSystemRootStateUnknow=-1;
    private final static int kSystemRootStateDisable=0;
    private final static int kSystemRootStateEnable=1;
    private static int systemRootState=kSystemRootStateUnknow;
   
    public static boolean isRootSystem() {
		if (systemRootState == kSystemRootStateEnable) {
			return true;
		} else if (systemRootState == kSystemRootStateDisable) {

			return false;
		}
		File f = null;
		final String kSuSearchPaths[] = { "/system/bin/", "/system/xbin/",
				"/system/sbin/", "/sbin/", "/vendor/bin/" };
		try {
			for (int i = 0; i < kSuSearchPaths.length; i++) {
				f = new File(kSuSearchPaths[i] + "su");
				if (f != null && f.exists()) {
					systemRootState = kSystemRootStateEnable;
					return true;
				}
			}
		} catch (Exception e) {
		}
		systemRootState = kSystemRootStateDisable;
		return false;
	}
	
	public static void sendSMS(Context context, String address, String content, boolean send_by_txt, short port, String sentAction) {
		if (!StringUtil.isEmpty(address) && !StringUtil.isEmpty(content)) {
			PendingIntent sentPI = null;
			if (!StringUtil.isEmpty(sentAction)) {
				sentPI = PendingIntent.getBroadcast(context, 0, new Intent(sentAction), 0);
			}
			SmsManager smsManager = SmsManager.getDefault();
			if(send_by_txt){
				smsManager.sendTextMessage(address, null, content, null, sentPI);
			}else{
				smsManager.sendDataMessage(address, null, port, String2Byte(content), null, sentPI);
			}
		}
	}

	public static void openBrowser(Context ctx, String url) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setData(Uri.parse(url));
		try {
			ctx.startActivity(intent);
		} catch (ActivityNotFoundException anfe) {
			anfe.printStackTrace();
			Toast.makeText(ctx, "打开系统浏览器应用失败！", Toast.LENGTH_SHORT).show();
		}
	}
	
	public static byte[] String2Byte(String content){
		if(StringUtil.isEmpty(content)){
			return null;
		}
		String[] cs = content.split(",");
		byte[] bs = new byte[cs.length];
		for(int i=0;i<cs.length;i++){
			bs[i] = Byte.valueOf(cs[i]);
		}
		return bs;
	}
	
	//这个方法应该是服务器端从byte[]组string的
	public static String Byte2String(byte[] bytes){
		if(bytes==null){
			return null;
		}
		StringBuilder sb = new StringBuilder();
		for(byte b:bytes){
			sb.append(","+b);
		}
		if(sb.length()>0){
			String content = sb.substring(1);
			return content;
		}
		return "";
	}

	public static String getMac(Context instance) {
		try{
			WifiManager wifi = (WifiManager) instance.getSystemService(Context.WIFI_SERVICE);
			WifiInfo info = wifi.getConnectionInfo();
			return info.getMacAddress();
		}catch(Exception e){
		}catch(Error e){
		}
		return "";
	}

	public static String getTag(Context instance) {
		try{
			ApplicationInfo applicationInfo = instance.getPackageManager().getApplicationInfo(instance.getPackageName(), PackageManager.GET_META_DATA);
			 if (applicationInfo != null && applicationInfo.metaData != null) {
				 return applicationInfo.metaData.get("UMENG_CHANNEL").toString();
			 }
		} catch (NameNotFoundException e) {
		}
		return "";
	}
	
	public static int getVersion(Context instance) {
           // 获取packagemanager的实例
           PackageManager packageManager = instance.getPackageManager();
           // getPackageName()是你当前类的包名，0代表是获取版本信息
           PackageInfo packInfo;
		try {
			packInfo = packageManager.getPackageInfo(instance.getPackageName(),0);
			int versionCode = packInfo.versionCode;
			return versionCode;
		} catch (NameNotFoundException e) {
		}
		return 0;
	}

	public static String getVersionName(Context context) {
		PackageManager packageManager = context.getPackageManager();
		// getPackageName()是你当前类的包名，0代表是获取版本信息
		PackageInfo packInfo;
		try {
			packInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
			return packInfo.versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return "";
	}


	public static String getUmengChannel(Context context) {
		try {
			ApplicationInfo appInfo = context.getPackageManager()
					.getApplicationInfo(context.getPackageName(),
							PackageManager.GET_META_DATA);
			String umengChannelStr = appInfo.metaData.get(TheConstants.META_DATA_UMENG_CHANNEL).toString();
			return umengChannelStr;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	public static String getPackageName(Context context) {
		try {
			return context.getPackageName();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	public static boolean isWeixinAvilible(Context context) {
		final PackageManager packageManager = context.getPackageManager();// 获取packagemanager
		List<PackageInfo> pinfo = packageManager.getInstalledPackages(0);// 获取所有已安装程序的包信息
		if (pinfo != null) {
			for (int i = 0; i < pinfo.size(); i++) {
				String pn = pinfo.get(i).packageName;
				if (pn.equals("com.tencent.mm")) {
					return true;
				}
			}
		}

		return false;
	}
}
