package com.wangniu.sevideo.util;

import android.content.Context;
import android.media.AudioManager;

/**
 * Created by Administrator on 2016/11/9 0009.
 */
public class VolumeTool {

    /**
    获取铃声最大音量
     */
    public static int getMaxVolume(Context context){
        AudioManager mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        int max = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_RING);
        return max;
    }
    /**
     获取铃声最大音量
     */
    public static int getCurrentVolume(Context context){
        AudioManager mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        int current = mAudioManager.getStreamVolume(AudioManager.STREAM_RING);
        return current;
    }
    /**
     获取铃声最大音量
     */
    public static void setVolume(Context context,int num){
        AudioManager mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        mAudioManager.setStreamVolume(AudioManager.STREAM_RING,num,0);
    }
}
