package com.wangniu.sevideo.util;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.AudioManager;
import android.os.Build;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;

import com.wangniu.sevideo.bean.LocalContact;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/11/15 0015.
 */
public class CallUtils {

    public static String TAG="CallUtils==";
    //接听电话
    public static void answerPhoneHeadsethook(Context context, Intent intent) {
        Log.e(TAG,"coming==");
        String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
        if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
            Log.e(TAG,"coming==");
            String number = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
            Log.d(TAG, "Incoming call from: " + number);
            Intent buttonUp = new Intent(Intent.ACTION_MEDIA_BUTTON);
            buttonUp.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_HEADSETHOOK));
            try {

                context.sendOrderedBroadcast(buttonUp, "android.permission.CALL_PRIVILEGED");
                Log.d(TAG, "ACTION_MEDIA_BUTTON broadcasted...");
            } catch (Exception e) {
                Log.d(TAG, "Catch block of ACTION_MEDIA_BUTTON broadcast !");
            }
            Intent headSetUnPluggedintent = new Intent(Intent.ACTION_HEADSET_PLUG);
            headSetUnPluggedintent.addFlags(Intent.FLAG_RECEIVER_REGISTERED_ONLY);
            headSetUnPluggedintent.putExtra("state", 1); // 0 = unplugged  1 = Headset with microphone 2 = Headset without microphone
            headSetUnPluggedintent.putExtra("name", "Headset");
            // TODO: Should we require a permission?
            try {
                context.sendOrderedBroadcast(headSetUnPluggedintent, null);
                Log.d(TAG, "ACTION_HEADSET_PLUG broadcasted ...");
            } catch (Exception e) {
                // TODO Auto-generated catch block
                Log.d(TAG, "Catch block of ACTION_HEADSET_PLUG broadcast");
                Log.d(TAG, "Call Answered From Catch Block !!");
            }
            Log.d(TAG, "Answered incoming call from: " + number);
        }
        Log.d(TAG, "Call Answered using headsethook");
    }

    //挂断电话
    public static void disconnectPhoneItelephony(Context context) {
        Log.i(context.getClass().getSimpleName(), "disconnectPhoneItelephony");
        TelephonyManager telephony = (TelephonyManager)
                context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            Class c = Class.forName(telephony.getClass().getName());
            Method m = c.getDeclaredMethod("getITelephony");
            m.setAccessible(true);
            Object iTelephony = m.invoke(telephony);
            Method m2 = iTelephony.getClass().getDeclaredMethod("endCall");
            m2.invoke(iTelephony);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void answerCall(Context context) {
        AudioManager localAudioManager = (AudioManager) context
                .getSystemService(Context.AUDIO_SERVICE);
        int i2 = 1;
        int i1 = 79;
        boolean bool1 = localAudioManager.isWiredHeadsetOn();
        if (!bool1) {
            Intent localIntent3 = new Intent(
                    "Android.intent.action.HEADSET_PLUG");
            localIntent3.putExtra("state", 1);
            localIntent3.putExtra("microphone", 0);
            localIntent3.putExtra("name", "");
            context.sendBroadcast(localIntent3);
            Intent meida_button = new Intent(
                    "android.intent.action.MEDIA_BUTTON");
            KeyEvent localKeyEvent2 = new KeyEvent(i2, i1);
            meida_button.putExtra("android.intent.extra.KEY_EVENT",
                    localKeyEvent2);
            context.sendOrderedBroadcast(meida_button, null);
            Intent headset_plug = new Intent(
                    "android.intent.action.HEADSET_PLUG");
            headset_plug.putExtra("state", 0);
            headset_plug.putExtra("microphone", 0);
            headset_plug.putExtra("name", "");
            context.sendBroadcast(headset_plug);
        } else {
            Intent meida_button = new Intent(
                    "android.intent.action.MEDIA_BUTTON");
            KeyEvent localKeyEvent1 = new KeyEvent(i2, i1);
            meida_button.putExtra("android.intent.extra.KEY_EVENT",
                    localKeyEvent1);
            context.sendOrderedBroadcast(meida_button, null);
        }
    }

    //获取通讯录联系人
    public static List<LocalContact> getContats(Context mContext){
        List<LocalContact> list = new ArrayList<LocalContact>();
        ContentResolver cr = mContext.getContentResolver(); //Activity/Application android.content.Context
        Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        if(cursor!=null&&cursor.moveToFirst())
        {
//            ArrayList<String> alContacts = new ArrayList<String>();
//            List<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
            do
            {
                LocalContact localContact = new LocalContact();
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                localContact.setId(Integer.valueOf(id));
                if(Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0)
                {
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",new String[]{ id }, null);
                    while (pCur.moveToNext())
                    {
                        String contactNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        contactNumber = StringUtil.getStringNoBlank(contactNumber);
                        localContact.setNum(contactNumber);
                        String name = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                        localContact.setName(name);
//                        Map<String,String> map = new HashMap<>();
//                        map.put("phone",contactNumber);
//                        map.put("name", name);
                        list.add(localContact);
                        break;
                    }
                    pCur.close();
                }

            } while (cursor.moveToNext()) ;
        }
        return list;
    }

}
