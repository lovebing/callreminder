package com.wangniu.sevideo.util;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {

	public static String format(String versionName) {
		return isEmpty(versionName)?"":versionName;
	}
	
	public static boolean isEmpty(String str){
		return str==null || str.trim().length()==0;
	}
	
	public static boolean isEmpty(Object obj){
		return obj==null;
	}
	
	public static boolean isEmpty(List list){
		return list==null || list.size()==0;
	}
	
	public static final boolean isSame(String value1, String value2) {
		if (isEmpty(value1) && isEmpty(value2))
			return true;
		if (!isEmpty(value1) && !isEmpty(value2))
			return value1.trim().equals(value2.trim());
		else
			return false;
	}
	
	public static final boolean isInt(String value) {
		if (isEmpty(value))
			return false;
		try {
			Integer.parseInt(value);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}
	
	public static final boolean isLong(String value) {
		if (isEmpty(value))
			return false;
		try {
			Long.parseLong(value);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}
	public static final boolean isDouble(String value) {
		if (isEmpty(value))
			return false;
		try {
			Double.parseDouble(value);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}

	public static final boolean isNumber(String text) {
		String regex = "[0-9]+$";
		return validate("[0-9]+$", text);
	}

	public static final boolean isEmail(String text) {
		String regex = "[(\\w-)+\\.]+@{1}[(\\w-)+\\.]+[a-z]{2,3}$";
		return validate("[(\\w-)+\\.]+@{1}[(\\w-)+\\.]+[a-z]{2,3}$", text);
	}

	public static final boolean isUrl(String text) {
		String regex = "(http://)?[(\\w-)+\\./]+[a-z]{2,3}[/(\\w-\\.)+]*";
		return validate("(http://)?[(\\w-)+\\./]+[a-z]{2,3}[/(\\w-\\.)+]*",
				text);
	}

	public static final boolean validate(String regex, String text) {
		Pattern pattern = Pattern.compile(regex, 2);
		return pattern.matcher(text).matches();
	}
	
	public static String encodeUrl(String str, String enc) {
		try {
			return isEmpty(str) ? "" : java.net.URLEncoder.encode(str,enc);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return str;
	}
	
	public static String subString(String str, int end) {
		if (null == str || end < 0) {
			return "";
		}
		return str.length() > end ? str.substring(0, end) : str;
	}
	
	public static String dateString2String(String str, String format) {
		try {
			return dataFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(str), format);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return "";
	}
	
	public static String date2String(Date _date) {
		if (null == _date) {
			_date = Calendar.getInstance().getTime();
		}
		
		return dataFormat(_date);
	}
	
	public static String dataFormat(Date date) {
		return dataFormat(date, "yyyy-MM-dd HH:mm:ss");
	}
	
	public static String dataFormat(Date date, String format) {
		return null == date ? "" :  new SimpleDateFormat(format).format(date);
	}
	
	public static Double string2DoubleScale(String str, int scale) {
		return isEmpty(str) ? 0.0 : new BigDecimal(str).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue() / 100;
	}
	
	public static Integer toDoubleScale(String str, int scale) {
		if (isEmpty(str) || scale < 1) {
			return 0;
		}
		
		Double _double = new BigDecimal(str).setScale(scale, BigDecimal.ROUND_HALF_UP).doubleValue() * 100;
		return _double.intValue();
	}
	
	public static String byte2XB(float b) {
    	int i = 1 << 10;
    	if(b < i) {
    		return b + "B";
    	}
    	i = 1 << 20;
    	if(b < i)
    		return calXB(b / (1 << 10)) + "KB";
    	i = 1 << 30;
    	if(b < i)
    		return calXB(b / (1 << 20)) + "MB";
    	return b + "B";
    }
    
    private static String calXB(float r) {
    	String result = r + "";
    	int index = result.indexOf(".");
		String s = result.substring(0, index + 1);
		String n = result.substring(index + 1);
		if(n.length() > 2)
			n = n.substring(0, 2);
		return s + n;
    }
	
	public static final float toFloat(Object val, float def) {
		if (val == null) {
			return def;
		}
		
		try {
			return Float.parseFloat(val.toString().trim());
		} catch (RuntimeException e) {
		}
		
		return 0f;
	}
	
	public static final int toInt(Object val, int def) {
		if (val == null) {
			return def;
		}
		
		try {
			return Integer.parseInt(val.toString().trim());
		} catch (RuntimeException e) {
			return def;
		}
	}
	
	public static final String toStr(Object val, String def) {
		if (val == null)
			return def;

		try {
			return val.toString().trim();
		} catch (RuntimeException e) {
			return def;
		}
	}
	
	public static String iso2Gbk(String str) {
		try {
			return new String(str.getBytes("iso-8859-1"), "GBK");
		} catch (Exception e) { }
		
		return "";
	}
	
	public static String gbk2iso(String str) {
		try {
			return new String(str.getBytes("GBK"), "iso-8859-1");
		} catch (Exception e) { }
		
		return "";
	}
	
	public static String maskNull(String str) {
		return (null == str ? "" : str);
	}

	public static final String maskUrl(String strUrl) {
		if (isEmpty(strUrl)) {
			return "";
		}
		String url = strUrl.trim().replaceAll("&amp;", "&");
		url = url.replaceAll(" ", "%20").trim();
		if (isEmpty(url))
			return "";
		if (!url.startsWith("http://")) {
			url = "http://" + url;
			return url;
		}
		return url;
	}
	
	public static boolean isSpeSign(String str) {
		if (isEmpty(str)) {
			return false;
		}
		
		Pattern pattern = Pattern.compile("[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]");
		Matcher mat = pattern.matcher(str);
		
		return mat.find();
	}

	public static String getMd5Value(String sSecret) {
		try {
			MessageDigest bmd5 = MessageDigest.getInstance("MD5");
			bmd5.update(sSecret.getBytes());
			int i;
			StringBuffer buf = new StringBuffer();
			byte[] b = bmd5.digest();
			for (int offset = 0; offset < b.length; offset++) {
				i = b[offset];
				if (i < 0)
					i += 256;
				if (i < 16)
					buf.append("0");
				buf.append(Integer.toHexString(i));
			}
			return buf.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}


	final static char[] digits = {
			'0', '1', '2', '3', '4', '5', '6', '7',
			'8','9', 'A', 'B', 'C', 'D', 'E', 'F',
			'G', 'H','J', 'K', 'L','M', 'N',  'P',
			'R', 'S', 'T', 'U', 'W', 'X', 'Y','Z' };

	public static String convertTo32(long input) {
		long num = 0;
		if (input < 0) {
			num = ((long) 2 * 0x7fffffff) + input + 2;
		} else {
			num = input;
		}
		char[] buf = new char[32];
		int charPos = 32;
		while ((num / 32) > 0) {
			buf[--charPos] = digits[(int) (num % 32)];
			num /= 32;
		}
		buf[--charPos] = digits[(int) (num % 32)];
		return new String(buf, charPos, (32 - charPos));
	}
	//font-sizes适配
	public static int getFontSize(Context context, int textSize) {
		DisplayMetrics dm = new DisplayMetrics();
		WindowManager windowManager = (WindowManager)
				context.getSystemService(Context.WINDOW_SERVICE);
		windowManager.getDefaultDisplay().getMetrics(dm);
		int screenHeight = dm.heightPixels;
		int rate;
		rate = (int) ((textSize
				* (float) screenHeight) / 1280);
		return rate;
	}
	/*
	去除空格
	 */
	public static String getStringNoBlank(String str) {
		if(str!=null && !"".equals(str)) {
			Pattern p = Pattern.compile("\\s*|\t|\r|\n");
			Matcher m = p.matcher(str);
			String strNoBlank = m.replaceAll("");
			return strNoBlank;
		}else {
			return str;
		}
	}
}
