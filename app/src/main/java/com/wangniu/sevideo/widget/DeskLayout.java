package com.wangniu.sevideo.widget;

import android.app.Fragment;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.wangniu.sevideo.MyApplication;
import com.wangniu.sevideo.R;
import com.wangniu.sevideo.bean.Contact;
import com.wangniu.sevideo.bean.DownloadInfo;
import com.wangniu.sevideo.db.FileManager;
import com.wangniu.sevideo.util.CallUtils;
import com.wangniu.sevideo.util.MeasureUtil;
import com.wangniu.sevideo.util.TheConstants;
import com.wangniu.sevideo.util.VolumeTool;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2016/11/14 0014.
 */
public class DeskLayout extends LinearLayout {

    private SurfaceView sfVideo;
    private int position;
    private MediaPlayer mediaPlayer;
    private TextView tvNum;
//    private SeekBar mSeekBar;
    private final int CYCLE = 10086;
    public static final int CLOSEWINDOWN = 10089;
    public static final int ANSWERCALL = 10078;
    public static final int ENDCALL = 10128;
    private final int UPDATNUM = 16789;
    private String num;
    private VideoView videoView;
    private Context mContext;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == UPDATNUM){
                Log.e("coming",num+"===coming===");
                tvNum.setText(num);
            }
        }
    };

    public DeskLayout(Context context, final Handler handler, String num) {
        super(context);
        mContext = context;
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
//                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
//                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
//                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        this.num=num;
        setOrientation(LinearLayout.VERTICAL);
        this.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        //initView
        View view = LayoutInflater.from(context).inflate(R.layout.item_desk, null);
//        sfVideo = (SurfaceView) view.findViewById(R.id.sv_video);
        videoView = (VideoView) view.findViewById(R.id.videoView);
//        FrameLayout flContent = (FrameLayout) view.findViewById(R.id.fl_content);
        TextView btnGet = (TextView) view.findViewById(R.id.btn_get);
        btnGet.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.sendEmptyMessage(ANSWERCALL);
            }
        });
        TextView btnEnd = (TextView) view.findViewById(R.id.btn_end);
        btnEnd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.sendEmptyMessage(ENDCALL);
            }
        });
        tvNum = (TextView) view.findViewById(R.id.tv_phonenum);
        tvNum.setText(num);
        this.addView(view);
        inintVideo();
//        checkPhoneNum(num);
    }

    private SurfaceHolder holder;

    private String uri;
    private void inintVideo() {
//        mediaPlayer = new MediaPlayer();
//        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//            @Override
//            public void onPrepared(MediaPlayer mp) {
//                mHandler.sendEmptyMessage(CYCLE);
//            }
//        });
//        try {
//            holder = sfVideo.getHolder();
//            holder.addCallback(new MyCallBack());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        AudioManager audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
//        audioManager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
        audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
//        currentVolum = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        int maxCurrent = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
////        Log.e("coming==", audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) + "***" + audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        int precent = MyApplication.getSharedPreferences().getInt("voice_precent",70);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, maxCurrent*precent/100, 0);

        //videoview
        if (num!=null){
            Contact contact = FileManager.queryContactSelect(num);
            Log.e("==downloadinfo==",num+"***"+(contact==null));
            Contact contact1 = FileManager.queryContactSelect("+86" + num);
            String name = FileManager.queryContact(num);
            String name1 = FileManager.queryContact("+86"+num);
            if (contact!=null && !"".equals(contact.getName())){
                num = contact.getName();
                tvNum.setText(num);
                String sId = contact.getsId();
                Log.e("==id==[1]",sId+"***");
                DownloadInfo downloadInfo = FileManager.queryById(Integer.valueOf(sId));
                if (downloadInfo!=null){
                    uri = downloadInfo.getUrl();
                }else{
                    uri = "android.resource://"+mContext.getPackageName() +"/"+R.raw.dvideo;
                }
            } else if (contact1!=null && !"".equals(contact1.getName())){
                num = contact1.getName();
                tvNum.setText(num);
                String sId = contact.getsId();

                DownloadInfo downloadInfo = FileManager.queryById(Integer.valueOf(sId));
                if (downloadInfo!=null){
                    uri = downloadInfo.getUrl();
                }else{
                    uri = "android.resource://"+mContext.getPackageName() +"/"+R.raw.dvideo;
                }
         }else{
              if (!"".equals(name)){
                  tvNum.setText(name);
              }else if (!"".equals(name1)){
                  tvNum.setText(name1);
              }
            DownloadInfo info = FileManager.queryById(MyApplication.getSharedPreferences().getInt(TheConstants.SELECTID, 0));
            Log.e("==id==[2]",MyApplication.getSharedPreferences().getInt(TheConstants.SELECTID, 0)+"***");
            if (info!=null){
                uri = info.getUrl();
            }else{
                uri = "android.resource://"+mContext.getPackageName() +"/"+R.raw.dvideo;
            }
        }
//        if ("default".equals(MyApplication.getSharedPreferences().getString(TheConstants.RINGSAVEPATH, ""))){
//            uri = "android.resource://"+mContext.getPackageName() +"/"+R.raw.dvideo;
//        }else{
//            DownloadInfo info = FileManager.queryById(MyApplication.getSharedPreferences().getInt(TheConstants.SELECTID, 0));
//            if (info!=null){
//                uri = info.getUrl();
//            }
//        }
        videoView.setVideoURI(Uri.parse(uri));
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                videoView.start();
            }
        });
        videoView.setOnInfoListener(new MediaPlayer.OnInfoListener() {
            @Override
            public boolean onInfo(MediaPlayer mp, int what, int extra) {
                switch (what) {
                    case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                        break;
                    case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                        break;
                }
                return false;
            }
        });
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                videoView.setVideoURI(Uri.parse(uri));
            }
        });
    }


//    private class MyCallBack implements SurfaceHolder.Callback {
//        @Override
//        public void surfaceCreated(SurfaceHolder holder) {
////            player = new MediaPlayer();
//            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
//            mediaPlayer.setDisplay(holder);
//            // 设置显示视频显示在SurfaceView上
//            try {
////                AssetFileDescriptor afd = mContext.getAssets().openFd("test.mp4");
////                mediaPlayer.setDataSource(afd.getFileDescriptor(),afd.getStartOffset(), afd.getLength());//
//                if ("default".equals(MyApplication.getSharedPreferences().getString(TheConstants.RINGSAVEPATH, ""))){
//                    Log.e("==coming==",MyApplication.getSharedPreferences().getString(TheConstants.RINGSAVEPATH, "")+"***");
//                    AssetFileDescriptor afd = mContext.getResources().openRawResourceFd(R.raw.dvideo);
//                    mediaPlayer.setDataSource(afd.getFileDescriptor(),afd.getStartOffset(), afd.getLength());
//                }else{
//                    DownloadInfo info = FileManager.queryById(MyApplication.getSharedPreferences().getInt(TheConstants.SELECTID, 0));
//                    Log.e("==coming==",info.getUrl()+"***");
//                    if (info!=null){
//                        mediaPlayer.setDataSource(info.getUrl());
//                    }
//                }
//                mediaPlayer.setLooping(true);
//                mediaPlayer.prepare();
//                mediaPlayer.start();
//                Log.e("playerDuration", mediaPlayer.getDuration() + "");
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//
//        @Override
//        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
//
//        }
//
//        @Override
//        public void surfaceDestroyed(SurfaceHolder holder) {
//
//        }
//    }
//    public void release(){
//        if (mediaPlayer!=null){
//            mediaPlayer.stop();
////            mediaPlayer.release();
//        }
//    }

//    private void checkPhoneNum(final String phoneNumber) {
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                if (phoneNumber!=null){
//                    String name = FileManager.queryContact(phoneNumber);
//                    String name1 = FileManager.queryContact("+86"+phoneNumber);
//                    if (!"".equals(name)){
//                        num = name;
//                        mHandler.sendEmptyMessage(UPDATNUM);
//                    }else if (!"".equals(name1)){
//                        num = name;
//                        mHandler.sendEmptyMessage(UPDATNUM);
//                    }
//                }
//            }
//        }).start();
//
    }



}
