package com.wangniu.sevideo.request;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.Map;

/**
 * Customized JSON Object Request of Volley
 *
 * Avoid the problem of fill post data to JsonObjectRequest
 *
 * Created by jason on 10/20/15.
 */
public class CallJSONObjectRequest extends Request<JSONObject> {

    private static final int NN_DEFAULT_TIMEOUT_MS = 20000; //20 seconds

    private Response.Listener<JSONObject> listener;
    private Map<String, String> params;

    public CallJSONObjectRequest(String url, Map<String, String> params,
                                 Response.Listener<JSONObject> reponseListener,
                                 Response.ErrorListener errorListener) {
        super(Method.GET, url, errorListener);
        this.listener = reponseListener;
        this.params = params;
    }

    public CallJSONObjectRequest(int method, String url, Map<String, String> params,
                                 Response.Listener<JSONObject> reponseListener,
                                 Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        this.listener = reponseListener;
        this.params = params;
        setRetryPolicy(new DefaultRetryPolicy(
                NN_DEFAULT_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    protected Map<String, String> getParams()
            throws com.android.volley.AuthFailureError {
        return checkParams(params);
    }

    private Map<String, String> checkParams(Map<String, String> map) {
        Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
        while(it.hasNext()) {
            Map.Entry<String, String> pair = it.next();
            if(pair.getValue() == null) {
                map.put(pair.getKey(), "");
            }
        }
        return map;
    }

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            return Response.success(new JSONObject(jsonString),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException je) {
            return Response.error(new ParseError(je));
        }
    }

    @Override
    protected void deliverResponse(JSONObject response) {
        listener.onResponse(response);
    }
}
