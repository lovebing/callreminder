package com.wangniu.sevideo.request;

import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.wangniu.sevideo.MyApplication;
import com.wangniu.sevideo.R;
import com.wangniu.sevideo.util.JSONUtil;
import com.wangniu.sevideo.util.TheConstants;

import org.json.JSONObject;

import java.util.Map;


/**
 * Created by Administrator on 2016/11/16 0016.
 */
public class RequestManager {

    public static void getDownloadInfo(){
        Map<String, String> reqParams = CallRequestUtils.getDownloadInfoParams();
        CallJSONObjectRequest objRequest = new CallJSONObjectRequest(Request.Method.POST,
                TheConstants.URL_GET_VIDEO_INFOS, reqParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        JSONObject[] dataArray = JSONUtil.getJSONArray2(jsonObject, "data");
                        if(dataArray != null) {
                            for(JSONObject data : dataArray) {
                                String strGoodsName = JSONUtil.getString(data, "goods_name");
                                String urlGoodsDetail = JSONUtil.getString(data, "goods_detail");
                                String urlGoodsImg = JSONUtil.getString(data, "goods_img");
                                int goodsId = JSONUtil.getInt(data, "goods_id");
                                int goodsRoundId = JSONUtil.getInt(data, "goods_round_id");
                                int goodsTotal = JSONUtil.getInt(data, "total");
                                int goodsSold = JSONUtil.getInt(data, "count_sold");
                                int goodsRound = JSONUtil.getInt(data, "goods_round");
                                int goodsStatus = JSONUtil.getInt(data, "status");
                                int goodsMinimum = JSONUtil.getInt(data, "pay_min");
                                int goodsDefault = JSONUtil.getInt(data, "pay_default");
                                int goodsSteps = JSONUtil.getInt(data, "pay_step");
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        if(volleyError instanceof TimeoutError) {
                            Toast.makeText(MyApplication.getInstance(),
                                    MyApplication.getInstance().getString(R.string.error_network_timeout), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MyApplication.getInstance(),
                                    MyApplication.getInstance().getString(R.string.error_network_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                });
        MyApplication.getInstance().addToRequestQueue(objRequest, "");
    }
}
