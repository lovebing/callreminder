package com.wangniu.sevideo.request;

import android.content.SharedPreferences;

import com.wangniu.sevideo.MyApplication;
import com.wangniu.sevideo.util.AndroidUtil;
import com.wangniu.sevideo.util.TheConstants;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2016/11/16 0016.
 */
public class CallRequestUtils {

    public static Map<String, String> getDownloadInfoParams() {
        Map<String,String> params = new HashMap<String,String>();
        return params;
    }

    public static Map<String, String> getYyscProductParams(
            final String openId, final String appVer) {
        Map<String,String> params = new HashMap<String,String>();
        params.put("cmd","12");
        params.put("wx_open_id", openId);
        params.put("random", Long.toString(System.currentTimeMillis()));
        params.put("channel", "");
        params.put("app_version", appVer);
        return params;
    }

    public static Map<String, String> getVipBuyParams(
            final String userId, final String openId, final int vipType) {
        Map<String,String> params = new HashMap<String,String>();
        params.put("cmd", "2");
        params.put("user_id", userId);
        params.put("wx_open_id", openId);
        params.put("vip_type", Integer.toString(vipType));
        extendCommonParams(params);
        return params;
    }

    private static Map<String, String> extendCommonParams(Map<String, String> params) {
        SharedPreferences lmPref = MyApplication.getInstance().getSharedPreferences();
        String token = lmPref.getString(TheConstants.LUCKY_MONEY_LOGIN_WX_AT, "");
        params.put("token", token);
        params.put("package_name", AndroidUtil.getPackageName(MyApplication.getInstance()));
        params.put("random", Long.toString(System.currentTimeMillis()));
        params.put("channel", "");
        params.put("app_version", Integer.toString(AndroidUtil.getVersion(MyApplication.getInstance())));
        return params;
    }


}
