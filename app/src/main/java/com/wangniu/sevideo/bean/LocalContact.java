package com.wangniu.sevideo.bean;

/**
 * Created by Administrator on 2016/12/29 0029.
 */
public class LocalContact {
    private int id;
    private String name;
    private String num;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }
}
