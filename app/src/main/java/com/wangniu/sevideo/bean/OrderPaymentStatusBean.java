package com.wangniu.sevideo.bean;

/**
 * Created by jason on 11/23/15.
 */
public class OrderPaymentStatusBean {

    public String mProductOrderId;
    public String mTradeOrderId;
    //wxpay required
    public String mOutTradeNo;
    public String mPrepareOrderId;
    public String mAppId;
    public String mMchId;
    public int mPaymentStatus;

    public OrderPaymentStatusBean() {

    }

    public OrderPaymentStatusBean(final String productOrderId, final String tradeOrderId,
                                  final String outTradeNo, final String prepareOrderId,
                                  final String appId, final String mchId) {
        this.mProductOrderId = productOrderId;
        this.mOutTradeNo = tradeOrderId;
        this.mOutTradeNo = outTradeNo;
        this.mPrepareOrderId = prepareOrderId;
        this.mAppId = appId;
        this.mMchId = mchId;
    }

    public void setPaymentStatus(final int status) {
        this.mPaymentStatus = status;
    }

    public String getmProductOrderId() {
        return mProductOrderId;
    }

    public String getmTradeOrderId() {
        return mTradeOrderId;
    }

    public String getmOutTradeNo() {
        return mOutTradeNo;
    }

    public String getmPrepareOrderId() {
        return mPrepareOrderId;
    }

    public String getmAppId() {
        return mAppId;
    }

    public String getmMchId() {
        return mMchId;
    }

    public int getmPaymentStatus() {
        return mPaymentStatus;
    }
}
