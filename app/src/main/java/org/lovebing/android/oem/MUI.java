package org.lovebing.android.oem;

import android.content.Context;
import android.content.Intent;

/**
 * Created by lovebing on 2017/12/10.
 */
public class MUI extends Vendor {

    public MUI(Context context) {
        super(context);
    }

    @Override
    public Name getName() {
        return Name.MUI;
    }

    @Override
    public void openAutoStartSetting() {
        Intent intent = new Intent();
        intent.setAction("miui.intent.action.OP_AUTO_START");
        intent.addCategory("android.intent.category.DEFAULT");
        context.startActivity(intent);
    }
}
