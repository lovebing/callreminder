package org.lovebing.android.oem;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;

/**
 * Created by lovebing on 2017/12/10.
 */
abstract public class Vendor {

    public enum Name {
        MUI,
        HAWEI,
        GENERAL
    }

    protected Context context;

    public Vendor(Context context) {
        this.context = context;
    }

    abstract public Name getName();
    abstract public void openAutoStartSetting();

    public void openNotificationSetting() {
        Intent i = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }

    public boolean notificationListenersEnabled() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return true;
        }
        return Settings.Secure.getString(context.getContentResolver(), "enabled_notification_listeners")
                .contains(context.getApplicationContext().getPackageName());
    }
}
