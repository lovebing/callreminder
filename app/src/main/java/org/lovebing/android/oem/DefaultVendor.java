package org.lovebing.android.oem;

import android.content.Context;

/**
 * Created by lovebing on 2017/12/10.
 */
public class DefaultVendor extends Vendor {

    public DefaultVendor(Context context) {
        super(context);
    }

    @Override
    public Name getName() {
        return Name.GENERAL;
    }

    @Override
    public void openAutoStartSetting() {

    }
}
