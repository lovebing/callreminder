package org.lovebing.android.oem;

import android.content.Context;
import android.os.Build;

/**
 * Created by lovebing on 2017/12/10.
 */
public class VendorFactory {

    private static Vendor vendor;

    public synchronized static Vendor getVendor(Context context) {
        if (vendor == null) {
            switch (Build.BRAND.toLowerCase()) {
                case "xiaomi":
                    vendor = new MUI(context);
                    break;
                default:
                    vendor = new DefaultVendor(context);
            }
        }
        return vendor;
    }
}
